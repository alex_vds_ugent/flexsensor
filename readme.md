Touch sensor

the sensor can be produced in various designs and with various materials. A piezoresistive material (e.g. Velostat or Eeontex fabric) is used as it's resistance changes if pressure is applied. The resistance is read out with a microcontroller using a simple voltage divider circuit.

![picture](doc/voltDiv.png)

To have multiple sensing points, a matrix can be created using crossing wires, with 'rows' and 'columns' on opposite sides of the piezoresistive material. The matrix is then read out one ADC at the time and one column at the time.

![picture](doc/sensorMatrix.png)