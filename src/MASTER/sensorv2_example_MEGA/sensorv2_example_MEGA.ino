/*
Based on https://www.kobakant.at/DIY/?p=7443
*/


#define sensorPoints numRows*numCols

int rows[] = {A2,A3,A1,A0}; //{A3, A2, A1, A0};
int cols[] = {12,9,8,10,13,11}; //{7,6,5,4,3,2};
const int LEN_ROWS = sizeof(rows) / sizeof(int);
const int LEN_COLUMNS = sizeof(cols) / sizeof(int);
//int incomingValues[sensorPoints] = {};
volatile int SensorValues[LEN_ROWS][LEN_COLUMNS];

void setup() {
  // set all rows and columns to INPUT (high impedance):
  for (int i = 0; i < LEN_ROWS; i++) { 
    pinMode(rows[i],INPUT);
  } 
  for (int i = 0; i < LEN_COLUMNS; i++) { 
    pinMode(cols[i], INPUT); 
  } 
  Serial.begin(9600);
  delay(500);
  Serial.println("Test...");


} 

void loop() {

  read_frame();
  print_frame();
  
  //Serial.println(); delay(500);
}

void read_frame(){
    for (int colCount = 0; colCount < LEN_COLUMNS; colCount++) { 
      pinMode(cols[colCount], OUTPUT); // set as OUTPUT 
      digitalWrite(cols[colCount], HIGH); // set high
      for (int rowCount = 0; rowCount < LEN_ROWS; rowCount++) {
        SensorValues[rowCount][colCount] = analogRead(rows[rowCount]); // read INPUT 
      }// end rowCount 
      pinMode(cols[colCount], INPUT); // set back to INPUT! 
  }// end colCount 
}

void print_frame() {
  for (int j = 0; j < LEN_ROWS; j++) {
    for (int k = 0; k < LEN_COLUMNS; k++) {
      Serial.print(SensorValues[j][k]);
      Serial.print(",");
    }
    Serial.println("");
  }

  Serial.println("");
}
