//tested on ubuntu_Dwenguino_arduinoIDE1.8.5_usbASP

//there is significant overhead with each I2C master-slave interaction using the wire library. It turns out (empirically) that only 15 bytes can be sent per I2C interaction (due to the wire library, not I2C). 
//Here, the 5x5 matrix is sent in two I2C interactions.  

#include <LiquidCrystal.h>
#include <Wire.h>
#include <Dwenguino.h>

//const int FRAME_RATE = 500; //Hz
const int N_INTERFLAGFRAMES = 100; // N frames between flag, flag used to synchronize Serial with pc
//volatile int delay_dur;
volatile uint8_t FlagCount =0;

const int ADDRESS_ARRAY[] = {35}; //30 35 38 
const int LEN_ADDRESS_ARRAY = sizeof(ADDRESS_ARRAY) / sizeof(int);
const int LEN_ROW = 5;
const int LEN_COLUMN = 5;
const int N_taxels = LEN_ROW*LEN_COLUMN;

byte received_data[LEN_ADDRESS_ARRAY][LEN_ROW][LEN_COLUMN];
  
void setup() {
  initDwenguino();
  dwenguinoLCD.clear();
  dwenguinoLCD.print("I2C on ");
  for (int i = 0; i < LEN_ADDRESS_ARRAY; i++) {
    dwenguinoLCD.print(ADDRESS_ARRAY[i]);
  }
  pinMode(13,OUTPUT); // for debugging
  Serial.begin(256000,SERIAL_8N1);
  delay(1000);
  Serial.flush();
  //delay_dur = 1000/FRAME_RATE;
  // init variabelen matrixen
  for (int i = 0; i < LEN_ADDRESS_ARRAY; i++) {
    for (int k = 0; k < LEN_COLUMN; k++) {
      for (int j = 0; j < LEN_ROW; j++) {
        received_data[i][j][k] = 66;
      }
    }
  }
  Wire.begin();
}

void loop() {
  digitalWrite(13,HIGH);
  pullData();  //pull data from sensors in address array
  digitalWrite(13,LOW);
  serialWriteFlag();
  serialWriteMatrix(0);
  delay(10);
}

void serialWriteFlag(){
  FlagCount+=1;
  if (FlagCount == N_INTERFLAGFRAMES){
    Serial.write(99);
    Serial.write(33);
    Serial.write(66);
    Serial.write(N_INTERFLAGFRAMES);
    FlagCount=0;
  }
}

//serial write to host computer
void serialWriteMatrix(int sensor) {
  for (int j = 0; j < LEN_ROW; j++) {
    for (int k = 0; k < LEN_COLUMN; k++) {
      Serial.write(received_data[sensor][j][k]);
    }
  }
}

//serial write to output monitor
void printMatrix(int sensor) {
  for (int j = 0; j < LEN_ROW; j++) {
    for (int k = 0; k < LEN_COLUMN; k++) {
      Serial.print(received_data[sensor][j][k]);
      Serial.print(",");
    }
    Serial.println("");
  }

  Serial.println("");
}

void pullData() {

  // iter sensors
  for (int i = 0; i < LEN_ADDRESS_ARRAY; i++) {
    //request data matrix
    //select row
    Wire.beginTransmission(ADDRESS_ARRAY[i]);
    Wire.write(0);
    Wire.endTransmission();
    Wire.requestFrom(ADDRESS_ARRAY[i], 15);//N_taxels

    for (int j = 0; j < 3; j++) { // iter rows
      for (int k = 0; k < LEN_COLUMN; k++) { // iter columns 
        byte v = Wire.read();     // receive pixel value
        //Serial.print(v);
        received_data[i][j][k] = v;

      }
    }

    Wire.beginTransmission(ADDRESS_ARRAY[i]);
    Wire.write(1);
    Wire.endTransmission();
    Wire.requestFrom(ADDRESS_ARRAY[i], 10);//N_taxels

    for (int j = 3; j < LEN_ROW; j++) { // iter rows
      for (int k = 0; k < LEN_COLUMN; k++) { // iter columns 
        byte v = Wire.read();     // receive pixel value
        //Serial.print(v);
        received_data[i][j][k] = v;

      }
    }
  }
}

