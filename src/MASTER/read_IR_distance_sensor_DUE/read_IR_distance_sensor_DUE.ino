
volatile int buff[] = {666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666};
const int LEN_BUFF = sizeof(buff) / sizeof(int);
const int N_AVG = 100; //20

unsigned long time0;
unsigned long time1;
unsigned long time2;
unsigned long time3;

void setup() {
  // initialize serial communication, DUE doesnt like 256000 baud rate, 128000 is fine
  //Serial.begin(9600); // for printing
  Serial.begin(128000,SERIAL_8N1);//19200
  delay(1000);
  Serial.flush();

}
 

void loop() {

  //time0 = micros();
  float avg = get_avg(N_AVG);
  //time1 = micros();
  //Serial.println(avg); //requires Serial.begin(9600);
  write_avg(avg);
}

float get_avg(int N){

  int sum =0;
  for (int i=0; i<N; i++){
    sum += analogRead(A0);
  }
  
  int avg = sum/N;
  float voltage = avg * (5.0 / 1023.0);

  return voltage;
}

void write_avg(float avg){
  int dist = avg*100; //should be in range [0,500]
  Serial.write(3); // 'flag'
  Serial.write(dist>>8); // HB
  Serial.write(dist); // LB
}

