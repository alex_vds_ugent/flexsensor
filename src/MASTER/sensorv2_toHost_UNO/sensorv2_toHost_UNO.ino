
//UNO has 10 bit ADC, here we remove the 2 LSB

int rows[] = {A3,A2,A1,A0};
int cols[] = {11,3,9,13,7,5}; 
const int LEN_ROWS = sizeof(rows) / sizeof(int);
const int LEN_COLUMNS = sizeof(cols) / sizeof(int);
volatile int SensorValues[LEN_ROWS][LEN_COLUMNS];
const int SENSORID = 27;
const int N_INTERFLAGFRAMES = 100; // N frames between flag, flag used to synchronize Serial with pc
volatile uint8_t FlagCount =0;


void setup() {

  // init serial
  Serial.begin(256000,SERIAL_8N1);
  //Serial.begin(9600);
  delay(1000);
  Serial.flush();

  void init_IO();
} 

void loop() {

  read_frame();
  //print_frame(); //requires Serial.begin(9600);
  write_flag(SENSORID);
  write_frame();

}

void read_frame(){
    for (int colCount = 0; colCount < LEN_COLUMNS; colCount++) { 
      pinMode(cols[colCount], OUTPUT); // set as OUTPUT 
      digitalWrite(cols[colCount], HIGH); // set high
      for (int rowCount = 0; rowCount < LEN_ROWS; rowCount++) {
        SensorValues[rowCount][colCount] = analogRead(rows[rowCount])>>2; // read INPUT 
      }// end rowCount 
      pinMode(cols[colCount], INPUT); // set back to INPUT! 
  }// end colCount 
  
}

void print_frame() {
  for (int j = 0; j < LEN_ROWS; j++) {
    for (int k = 0; k < LEN_COLUMNS; k++) {
      Serial.print(SensorValues[j][k]);
      Serial.print(",");
    }
    Serial.println("");
  }

  Serial.println("");
}

void write_flag(uint8_t sensorID){
  //Flag is used to synchronize serial communication and to transmit info
  FlagCount+=1;
  if (FlagCount == N_INTERFLAGFRAMES){
    Serial.write(99);
    Serial.write(33);
    Serial.write(66);
    Serial.write(N_INTERFLAGFRAMES);
    Serial.write(sensorID);
    Serial.write(LEN_ROWS);
    Serial.write(LEN_COLUMNS);
    FlagCount=0;
  }
}

void write_frame() {
  //write matrix to host
  for (int j = 0; j < LEN_ROWS; j++) {
    for (int k = 0; k < LEN_COLUMNS; k++) {
      Serial.write(SensorValues[j][k]);
    }
  }
}

void init_IO() {
  // set all rows and columns to INPUT (high impedance):
  for (int i = 0; i < LEN_ROWS; i++) { 
    pinMode(rows[i],INPUT);
  } 
  for (int i = 0; i < LEN_COLUMNS; i++) { 
    pinMode(cols[i], INPUT); 
  } 
}

