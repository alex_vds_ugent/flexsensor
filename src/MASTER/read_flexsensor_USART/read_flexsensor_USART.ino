//tested on ubuntu_Dwenguino_arduinoIDE1.8.5

//It turns out (empirically) that only 15 bytes can be sent per I2C interaction (due to the wire library, not I2C). 
//Here, the 5x5 matrix is sent in two I2C interactions.  

//#include <LiquidCrystal.h>
//#include <Wire.h>
//#include <Dwenguino.h> //not vital


const int ADRESS_ARRAY[] = {33}; //30 35 
const int LEN_ADRESS_ARRAY = sizeof(ADRESS_ARRAY) / sizeof(int);
const int LEN_ROW = 5;
const int LEN_COLUMN = 5;
const int N_taxels = LEN_ROW*LEN_COLUMN;

int received_data[LEN_ADRESS_ARRAY][LEN_ROW][LEN_COLUMN];

unsigned char fromTiny; 

unsigned int BAUD_PRESCALLER = 25; //0 for 1MHz

unsigned long int time0;
unsigned long int time1;

void setup() {

  //Serial.begin(9600);
  //delay(10);
  //Serial.println("Flexsensor test initialisation");
  /* USART init */
  /* Set baud rate */
   UBRR1H = (uint8_t)(BAUD_PRESCALLER>>8);
   UBRR1L = (uint8_t)(BAUD_PRESCALLER);
   /* Enable receiver and transmitter */
   UCSR1B = (1<<RXEN1)|(1<<TXEN1);
  /* Set frame format: 8data, 2stop bit */
  UCSR1C = (1<<USBS1)|(3<<UCSZ10);
  /*set as synchronous USART*/
  UCSR1C |= (1<<UMSEL10);
  UCSR1C &= ~(1<<UMSEL11);

  /*set XCK1 pin as intput (Slave mode, internal clock used) */
  DDRD &= ~(1<<PD5);
  
}

void loop() {
  //Serial.println("lets listen");
  //wait_for_start();
  time0 = micros();
  read_frame();
  time1 = micros();
  Serial.println("read_frame took: ");
  Serial.println(time1-time0);
  print_frame(0);
  //fromTiny = USART_receive();
  //Serial.println(fromTiny);
  delay(100);
}


void print_frame(int sensor) {
  for (int j = 0; j < LEN_ROW; j++) {
    for (int k = 0; k < LEN_COLUMN; k++) {
      Serial.print(received_data[sensor][j][k]);
      Serial.print(",");
    }
    Serial.println("");
  }

  Serial.println("");
}

void wait_for_start(){
  byte b = 1;
  Serial.println(b);
  while (1){
    byte b = USART_receive();
    Serial.println(b);
    if (b==252){
      //break;
    }
  }
  //Serial.println("out of while");
  return;
}

void read_frame(){

    for (int j = 0; j < LEN_ROW; j++) { // iter rows
      for (int k = 0; k < LEN_COLUMN; k++) { // iter columns 
        byte v = USART_receive();     // receive pixel value
        //Serial.print(v);
        received_data[0][j][k] = v;
      }
    }
}
 
unsigned char USART_receive(void){
 
 while(!(UCSR1A & (1<<RXC1)));
 return UDR1;
 
}
 
void USART_send( unsigned char data){
 
   while(!(UCSR1A & (1<<UDRE1)));
   UDR1 = data;
   
}
 
void USART_putstring(char* StringPtr){
 
  while(*StringPtr != 0x00){
     USART_send(*StringPtr);
     StringPtr++;}
 
}

