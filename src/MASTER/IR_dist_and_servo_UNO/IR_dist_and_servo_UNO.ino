unsigned int servo_delay_ms = 500;//delay between pos update, should be multiple of 10
unsigned char P_transm_ms = 5; //delay between sensor transmissions interrupts, in ms. (no float)
unsigned char ServoSensor = 1; // if not 0 servo position is considered a sensor (with ID 16)

volatile unsigned int servo_val = 25;
volatile unsigned char cur_servo_pos = 66;
unsigned int MIN_POS = 25;
unsigned int MAX_POS = 150;
volatile unsigned char ASCEND = 1;
volatile unsigned char count = 0;
volatile unsigned char count_send = P_transm_ms;
unsigned char META_TIMER;
unsigned char dist_ID = 49;
unsigned char servo_ID = 19;
volatile float avg;
volatile unsigned char distance=1;

#include <Servo.h>

Servo myservo;  // create servo object to control a servo


volatile int buff[] = {666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666};
const int LEN_BUFF = sizeof(buff) / sizeof(int);
const int N_AVG = 60; //10; // on UNO it takes approx 1.1 ms for avg of 10


//unsigned long time0;
//unsigned long time1;
//unsigned long time2;
//unsigned long time3;


void setup() {
  // initialize serial communication
  //Serial.begin(9600); // for printing
  Serial.begin(256000,SERIAL_8N1);
  set_servo(servo_val);
  myservo.attach(2);
  delay(1000);
  Serial.flush();

  
  //stop interrupts
  cli();
  
  ////set timer0 interrupt at 100Hz
  TCCR0A = 0;// set entire TCCR0A register to 0
  TCCR0B = 0;// same for TCCR0B
  TCNT0  = 0;//initialize counter value to 0
  // set compare match register for 100hz interupts
  OCR0A = 156; 
  // turn on CTC mode
  TCCR0A |= (1 << WGM01);
  // Set CS02 and CS00 bits for 1024 prescaler
  TCCR0B |= (1 << CS02) | (1 << CS00);   
  // enable timer compare interrupt
  //TIMSK0 |= (1 << OCIE0A);
  
  //set timer2 interrupt at 8kHz
  TCCR2A = 0;// set entire TCCR2A register to 0
  TCCR2B = 0;// same for TCCR2B
  TCNT2  = 0;//initialize counter value to 0
  // set compare match register for 1ms interrupts
  OCR2A = 250;//
  // turn on CTC mode
  TCCR2A |= (1 << WGM21);
  // Set CS21 bit for 64 prescaler
  TCCR2B |= (1 << CS22);   
  // enable timer compare interrupt
  //TIMSK2 |= (1 << OCIE2A);
  
  //allow interrupts
  sei();
  

  META_TIMER = servo_delay_ms/10;
}
 

void loop() {
  //continuously update avg sensor readout
  //avg = get_avg(N_AVG);
  update_avg(N_AVG);
  //Serial.println(avg);


  int avail=Serial.available();
  
  //Serial.write(avail);
  if (avail> 0) {

    int incomingByte = Serial.read();
    /*
    if (incomingByte==dist_ID){ //write dist sens value
      write_avg(avg);
    }

    else if (incomingByte==servo_ID){ //write dist sens value
      Serial.write(cur_servo_pos);
    }
    */
    if (incomingByte==6){ //start interrupts
      // enable servo timer compare interrupt
      TIMSK0 |= (1 << OCIE0A);
      // enable serial write timer compare interrupt
      TIMSK2 |= (1 << OCIE2A);
    }

    else if (incomingByte==7){ //end interrupts
      // disable servo timer compare interrupt
      TIMSK0 &= ~(1 << OCIE0A);
      // disable serial write timer compare interrupt
      TIMSK2 &= ~(1 << OCIE2A);
    }
    
    else if (incomingByte==8){ //reply ID
      if (ServoSensor){
        Serial.write(dist_ID+servo_ID); // inform host there are two sensors
      }
      else{
        Serial.write(dist_ID); //49 for distance, 16 for pressure, 19 for servo
      }
    }
    
    else if (incomingByte==5){ //reply ID
        Serial.write(P_transm_ms); // inform host there are two sensors
    }
  }

}



float get_avg(int N){

  int sum =0;
  for (int i=0; i<N; i++){
    int readval = analogRead(A0);
    //Serial.print(readval);
    //Serial.print(',');
    sum += readval;
  }
  int avg_readout = sum/N;
  float voltage = avg_readout * (5.0 / 1023.0);
  return voltage;
}

float update_avg(int N){
  avg = get_avg(N);
}

void write_avg(){
  Serial.write(66); //Flag
  int dist = avg*100; //tranform from [0.00,5.00] to [0,500]
  Serial.write(dist>>8); // HB
  Serial.write(dist); // LB
}


void set_servo(unsigned char val){
  
  if (val<MIN_POS){
    myservo.write(MIN_POS);
  }
  else if (val>MAX_POS){
    myservo.write(MAX_POS);
  }
  else {
    myservo.write(val);
  }
  cur_servo_pos = val;
}

ISR(TIMER2_COMPA_vect){
  count_send++;
  if (count_send==P_transm_ms){
    count_send=0;
    if (distance){
      write_avg();
      distance=0;
    }
    else{
      Serial.write(cur_servo_pos);
      distance=1;
    }
  }
}

ISR(TIMER0_COMPA_vect){
  if (count==META_TIMER){
    count =0;
    if (ASCEND){
      servo_val++;
      set_servo(servo_val);
    }
    else {
     servo_val--;
     set_servo(servo_val);
    }
  
    if (servo_val==MAX_POS){
      ASCEND=0;      
    }
    if (servo_val==MIN_POS){
      ASCEND=1;      
    }
  }
  else {
    count++;
  }
}

