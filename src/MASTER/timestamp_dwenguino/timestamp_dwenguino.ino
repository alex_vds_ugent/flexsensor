//tested on ubuntu_Dwenguino_arduinoIDE1.8.5_usbASP
#include <LiquidCrystal.h>
#include <Wire.h>
#include <Dwenguino.h>
//#include <sys/time.h>



const int ADRESS_ARRAY[] = {66}; //30 35 
const int LEN_ADRESS_ARRAY = sizeof(ADRESS_ARRAY) / sizeof(int);
const int LEN_ROW = 5;
const int LEN_COLUMN = 5;

int received_data[LEN_ADRESS_ARRAY][LEN_ROW][LEN_COLUMN];

unsigned long time0;
unsigned long time1;
unsigned long time2;
unsigned long time3;
unsigned long time4;
unsigned long time5;
unsigned long time6;
unsigned long time7;

void setup() {
  initDwenguino();
  pinMode(13,OUTPUT); // for debugging
  Serial.begin(9600);
  delay(10);
  Serial.println("Flexsensor test initialisation");

/*  // init variabelen matrixen
  for (int i = 0; i < LEN_ADRESS_ARRAY; i++) {
    for (int k = 0; k < LEN_COLUMN; k++) {
      for (int j = 0; j < LEN_ROW; j++) {
        received_data[i][j][k] = 1;
      }
    }
  }*/
  Wire.begin();
}

void loop() {
  digitalWrite(13,HIGH);
  time0 = micros();
  pullData();  //pull data from sensors in aadress array
  time1 = micros();
  digitalWrite(13,LOW);
  
  Serial.print("PullData took (us): ");
  Serial.println(time1-time0);
  Serial.print("MOSI first row took (us): ");
  Serial.println(time3-time2);
  Serial.print("MOSI first row request(adrress,row) took (us): ");
  Serial.println(time5-time4);
  
  //Serial.print("MISO last row first column took (us): ");
  //Serial.println(time4-time3);
  printMatrix(0);    
  printMatrix_ints(0);
/*  for (int k = 0; k < LEN_ADRESS_ARRAY; k++) {
    Serial.print("Data sensor: ");
    Serial.println(ADRESS_ARRAY[k]);
    printMatrix(k);

  }*/
  delay(20);
}



void printMatrix(int sensor) {
  for (int j = 0; j < LEN_ROW; j++) {
    for (int k = 0; k < LEN_COLUMN; k++) {
      Serial.print(received_data[sensor][j][k]);
      Serial.print(",");
    }
    Serial.println("");
  }

  Serial.println("");
}

void printMatrix_ints(int sensor) {
  for (int j = 0; j < LEN_ROW; j++) {
    int myint = 0;
    for (int k = 0; k < LEN_COLUMN; k++) {
      int val = received_data[sensor][j][k];
      val = val << 8*k;
      myint = myint+val;
    }
    Serial.print(myint);
    Serial.print(",");
    Serial.println("");
  }

  Serial.println("");
}




void pullData() {
  //int received_data[LEN_ADRESS_ARRAY][LEN_ROW][LEN_COLUMN];
  // iter sensors
  for (int i = 0; i < LEN_ADRESS_ARRAY; i++) {
    // iter rows
    
    for (int j = 0; j < LEN_ROW; j++) {
      if (j==0){
        time2 = micros();
      }
      //select row
      Wire.beginTransmission(ADRESS_ARRAY[i]);
      Wire.write(j);
      Wire.endTransmission();



      //get single row data
      if (j==0){
        time4 = micros();
      }
      Wire.requestFrom(ADRESS_ARRAY[i], LEN_COLUMN);
      if (j==0){
        time5 = micros();
      }
      for (int k = 0; k < LEN_COLUMN; k++) {
        
        time6 = micros();
        
        int v = Wire.read();     // receive pixel value
        
        
        time7 = micros();
        
        received_data[i][j][k] = time7-time6;
      }
      if (j==0){
        time3 = micros();
      }
    }
  }
  // print
  /*
  for (int i = 0; i < LEN_ADRESS_ARRAY; i++) {
    for (int j = 0; j < LEN_ROW; j++) {
      for (int k = 0; k < LEN_COLUMN; k++) {
        Serial.print(received_data[i][j][k]);
        Serial.print(",");
      }
      Serial.println("");
    }
    Serial.println("");
  }
  */
}

