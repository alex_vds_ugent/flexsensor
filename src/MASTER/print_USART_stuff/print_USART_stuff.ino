//tested on ubuntu_Dwenguino_arduinoIDE1.8.5_usbASP

//It turns out (empirically) that only 15 bytes can be sent per I2C interaction (due to the wire library, not I2C). 
//Here, the 5x5 matrix is sent in two I2C interactions.  

#include <LiquidCrystal.h>
#include <Wire.h>
//#include <Dwenguino.h> //not vital


const int ADRESS_ARRAY[] = {33}; //30 35 
const int LEN_ADRESS_ARRAY = sizeof(ADRESS_ARRAY) / sizeof(int);
const int LEN_ROW = 5;
const int LEN_COLUMN = 5;
const int N_taxels = LEN_ROW*LEN_COLUMN;

int received_data[LEN_ADRESS_ARRAY][LEN_ROW][LEN_COLUMN];

unsigned char fromTiny; 

unsigned int BAUD_PRESCALLER = 0; //0 for 1MHz

void setup() {
  //initDwenguino(); //not vital
  //pinMode(13,OUTPUT); // for debugging
  Serial.begin(9600);
  delay(10);
  Serial.println("Flexsensor test initialisation");
    /* USART init */
  /* Set baud rate */
   UBRR1H = (uint8_t)(BAUD_PRESCALLER>>8);
   UBRR1L = (uint8_t)(BAUD_PRESCALLER);
   /* Enable receiver and transmitter */
   UCSR1B = (1<<RXEN1)|(1<<TXEN1);
  /* Set frame format: 8data, 2stop bit */
  UCSR1C = (1<<USBS1)|(3<<UCSZ10);
  /*set as synchronous USART*/
  UCSR1C |= (1<<UMSEL10);
  UCSR1C &= ~(1<<UMSEL11);

  /*set XCK1 pin as intput (Slave mode, internal clock used) */
  DDRD &= ~(1<<PD5);
  
}

void loop() {
  Serial.println("lets listen");
  fromTiny = USART_receive();
  Serial.println(fromTiny);
  delay(100);
}

 
unsigned char USART_receive(void){
 
 while(!(UCSR1A & (1<<RXC1)));
 return UDR1;
 
}
 
void USART_send( unsigned char data){
 
   while(!(UCSR1A & (1<<UDRE1)));
   UDR1 = data;
   
}
 
void USART_putstring(char* StringPtr){
 
  while(*StringPtr != 0x00){
     USART_send(*StringPtr);
     StringPtr++;}
 
}

