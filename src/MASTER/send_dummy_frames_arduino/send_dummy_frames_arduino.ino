//sending dummy frames over serial to test sampling rate
//#include <LiquidCrystal.h>
#include <Wire.h>
//#include <Dwenguino.h>


const int FRAME_RATE = 500; //Hz
const int N_INTERFLAGFRAMES = 100; // N frames between flag, flag used to synchronize Serial with pc
volatile int delay_dur;
const int LEN_ROW = 5;
const int LEN_COLUMN = 5;
//const int N_taxels = LEN_ROW*LEN_COLUMN;
const int LEN_ADRESS_ARRAY = 1;
volatile uint8_t FlagCount =0;

byte received_data[LEN_ADRESS_ARRAY][LEN_ROW][LEN_COLUMN];
  
void setup() {
  //initDwenguino();
  //dwenguinoLCD.clear();
  //dwenguinoLCD.print("Dummy FR @ ");
  //dwenguinoLCD.print(FRAME_RATE);
  //dwenguinoLCD.print("Hz");
  //pinMode(13,OUTPUT); // for debugging
  Serial.begin(256000,SERIAL_8N1);
  delay(1000);
  Serial.flush();
  
  //Serial.println("Flexsensor test initialisation");
  delay_dur = 1000/FRAME_RATE;
  // init variabelen matrixen
  for (int i = 0; i < LEN_ADRESS_ARRAY; i++) {
    for (int k = 0; k < LEN_COLUMN; k++) {
      for (int j = 0; j < LEN_ROW; j++) {
        received_data[i][j][k] = 66;
      }
    }
  }
  //Wire.begin();
}

void loop() {
  //digitalWrite(13,HIGH);
  delay(delay_dur);  
  //digitalWrite(13,LOW);
  serialWriteFlag(26);
  serialWriteMatrix();

}

void serialWriteFlag(uint8_t sensorID){
  FlagCount+=1;
  if (FlagCount == N_INTERFLAGFRAMES){
    Serial.write(99);
    Serial.write(33);
    Serial.write(66);
    Serial.write(N_INTERFLAGFRAMES);
    Serial.write(sensorID);
    FlagCount=0;
  }
}
  
void serialWriteMatrix() {
  for (int j = 0; j < LEN_ROW; j++) {
    for (int k = 0; k < LEN_COLUMN; k++) {
      //Serial.write(received_data[sensor][j][k]);
      uint8_t c =88;
      Serial.write(c);
    }
  }
  //Serial.println("");
}


