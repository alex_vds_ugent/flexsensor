unsigned int servo_delay_ms = 500;//delay between pos update, should be multiple of 10
unsigned int MIN_POS = 25;
unsigned int MAX_POS = 150;
volatile unsigned int servo_val = 30;
volatile unsigned char ASCEND = 1;
volatile unsigned char count = 0;
unsigned char META_TIMER;

#include <Servo.h>

Servo myservo;  // create servo object to control a servo


volatile int buff[] = {666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666,666};
const int LEN_BUFF = sizeof(buff) / sizeof(int);
const int N_AVG = 60; //10; // on UNO it takes approx 1.1 ms for avg of 10


//unsigned long time0;
//unsigned long time1;
//unsigned long time2;
//unsigned long time3;


void setup() {
  // initialize serial communication
  //Serial.begin(9600); // for printing
  Serial.begin(256000,SERIAL_8N1);
  set_servo(servo_val);
  myservo.attach(2);
  delay(1000);
  Serial.flush();

  
  //stop interrupts
  cli();
  //set timer0 interrupt at 2kHz
  TCCR0A = 0;// set entire TCCR0A register to 0
  TCCR0B = 0;// same for TCCR0B
  TCNT0  = 0;//initialize counter value to 0
  // set compare match register for 100hz interupts
  OCR0A = 156; 
  // turn on CTC mode
  TCCR0A |= (1 << WGM01);
  // Set CS02 and CS00 bits for 1024 prescaler
  TCCR0B |= (1 << CS02) | (1 << CS00);   
  // enable timer compare interrupt
  //TIMSK0 |= (1 << OCIE0A);
  //allow interrupts
  sei();
  

  META_TIMER = servo_delay_ms/10;
  

}
 

void loop() {
  float avg = get_avg(N_AVG);
  write_avg(avg);
  //Serial.println(avg);
  
  int avail=Serial.available();
  //Serial.write(avail);
  if (avail> 0) {
    int incomingByte = Serial.read();
    if (incomingByte==6){ //start servo
      // enable timer compare interrupt
      TIMSK0 |= (1 << OCIE0A);
    }
    if (incomingByte==8){ //reply ID
      for (int i=0;i<100;i++){
        Serial.write(49); //49 for distance, 16 for pressure
      }
    }
  }  
}



float get_avg(int N){

  int sum =0;
  for (int i=0; i<N; i++){
    int readval = analogRead(A0);
    //Serial.print(readval);
    //Serial.print(',');
    sum += readval;
  }
  
  int avg = sum/N;
  float voltage = avg * (5.0 / 1023.0);

  return voltage;
}

void write_avg(float avg){
  int dist = avg*100; //tranform from [0.00,5.00] to [0,500]
  Serial.write(66); // 'flag'
  Serial.write(dist>>8); // HB
  Serial.write(dist); // LB
}


void set_servo(unsigned char val){
  
  if (val<MIN_POS){
    myservo.write(MIN_POS);
  }
  else if (val>MAX_POS){
    myservo.write(MAX_POS);
  }
  else {
    myservo.write(val);
  }
}

ISR(TIMER0_COMPA_vect){
  if (count==META_TIMER){
    count =0;
    if (ASCEND){
      servo_val++;
      set_servo(servo_val);
    }
    else {
     servo_val--;
     set_servo(servo_val);
    }
  
    if (servo_val==MAX_POS){
      ASCEND=0;      
    }
    if (servo_val==MIN_POS){
      ASCEND=1;      
    }
  }
  else {
    count++;
  }
}

