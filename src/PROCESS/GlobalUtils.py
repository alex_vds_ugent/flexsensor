import time
import numpy as np
import os
import shutil


def get_timestamp():
	"""
	returns timestamp in format 'yyyy-mm-dd-hh-mm-ss'
	"""
	localTime = time.localtime()[0:6]  # used to make savefolder with timestamp
	timestamp = "-".join([str(t) for t in localTime])
	return timestamp

def make_directory(experiment_name, directory=None):
	"""
	if directory not given this function will use hard coded paths
	"""
	
	#localTime = time.localtime()[0:6]  # used to make savefolder with timestamp
	#experiment_name = "-".join([str(t) for t in localTime]) + '_' + experiment_name
	timestamp = get_timestamp()
	experiment_name = timestamp + '_' + experiment_name
	if directory == None:	
		exp_dir = os.path.join(os.environ['HOME'],'Documents/ExperimentData/', experiment_name)
	elif os.path.isdir(directory):
		exp_dir = os.path.join(directory, experiment_name)	
	elif os.path.isdir( os.path.join(os.environ['HOME'],'Documents/ExperimentData/',directory) ): #if directory = subfolder name in ExperimentData
		exp_dir = os.path.join(os.environ['HOME'],'Documents/ExperimentData/', directory, experiment_name)
		
	os.mkdir(exp_dir)
	return exp_dir

def copy_executable_NRPplotter(dest_dir,source_dir=None):
	"""
	if source directory not given this function will use hard coded paths
	"""
	if source_dir:
		shutil.copy2(source_dir,dest_dir)
	else:
		shutil.copy2(os.environ['HOME']+'/Dropbox/UGent/Code/Python/clickMeForPlots.py', dest_dir+'/clickMeForPlots.py')
	return

def copy_executable_CMAESplotter(dest_dir,source_dir=None):
	"""
	if source directory not given this function will use hard coded paths
	"""
	if source_dir:
		shutil.copy2(source_dir,dest_dir)
	else:
		shutil.copy2(os.environ['HOME']+'/Dropbox/UGent/Code/Python/clickMeForPlotsCmaes.py', dest_dir+'/clickMeForPlots.py')
	return


def copy_this_file(exp_dir, filename):
	'''
	e.g. GlobalUtils.copy_this_file(exp_dir=self.exp_dir,filename=__file__)
	'''
        shutil.copy(filename, os.path.join(exp_dir,filename))
	return

