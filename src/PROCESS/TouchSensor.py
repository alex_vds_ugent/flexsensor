import serial
import serial.tools.list_ports
import time
import tables
import sys
import os
import numpy as np


import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import cm  # Color maps
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import Tkinter

import GlobalUtils as GU
import scipy as sp
import scipy.signal as sps
from scipy.interpolate import interp1d
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn import metrics

from IPython.core.debugger import Tracer


def Reload(dataFile):
        import TouchSensor
        reload(TouchSensor)
        obj=TouchSensor.Processor(dataFile)
        return obj

def analog2diff(current_frame, prev_frame,spike_threshold=1):

    """
    Convert analog frame to differential (spiking) frame by simple tresholding
    Increasing and decreasing spikes
    :param current_frame:
    :param prev_frame:
    :param spike_threshold:
    :return: spike matrix with (0,1,2) = (neg change, no change, pos change)
    """

    # NROWS, NCOLS = current_frame.shape[0],current_frame.shape[1]
    diff = current_frame.astype('int16') - prev_frame.astype('int16') #needs to be a signed and large enough type

    spike_matrix = np.ones(current_frame.shape)
    spike_matrix[diff >= spike_threshold] = 2 #increasing diff
    spike_matrix[diff <= -spike_threshold] = 0 #decreasing diff

    return spike_matrix.astype('uint8')  # uint8 for imshow

def diff2motion_reichardt(current_spike_matrix, prev_spike_matrix,direction='L2R',reichardt_delay=1):
    """
    Implement basic reichardt detector by comparing two subsequent spiking matrices
    :param current_spike_matrix:
    :param prev_spike_matrix:
    :return:
    """

    spike_matrix=np.zeros(current_spike_matrix.shape)

    print(prev_spike_matrix.shape)
    print(current_spike_matrix.shape)

    D = reichardt_delay #frames delay for reichardt detector
    if direction == 'D2U':
        diff=prev_spike_matrix[D:,:]+current_spike_matrix[:-D,:]
        spike_matrix[:-D,:] = 1
        spike_matrix[:-D,:][diff == 4] = 2
    elif direction == 'R2L':
        diff = prev_spike_matrix[:, D:]+current_spike_matrix[:, :-D]
        spike_matrix[:, :-D] = 1
        spike_matrix[:, :-D][diff == 4] = 2
    elif direction == 'L2R':
        diff=prev_spike_matrix[:,:-D]+current_spike_matrix[:,D:]
        spike_matrix[:,D:] = 1
        spike_matrix[:,D:][diff == 4] = 2
    elif direction == 'U2D':
        diff=prev_spike_matrix[:-D,:]+current_spike_matrix[D:,:]
        spike_matrix[D:,:] = 1
        spike_matrix[D:,:][diff == 4] = 2


    return spike_matrix

def diff2motion(current_spike_matrix,direction='L2R'):
    """
    Implement basic reichardt detector by comparing two subsequent spiking matrices
    :param current_spike_matrix:
    :param prev_spike_matrix:
    :return:
    """

    spike_matrix=np.zeros(current_spike_matrix.shape)

    D = 1 #reichardt delay

    if direction == 'D2U':
        spike_matrix[:-D,:] = 1
        if 2 in current_spike_matrix:
            X=np.where(current_spike_matrix==2)[0]
            Y=np.where(current_spike_matrix==2)[1]
            for x,y in zip(X,Y):
                if x+D<current_spike_matrix.shape[0]:
                    if current_spike_matrix[x+D,y]==0:
                        spike_matrix[x,y]=2
    elif direction == 'R2L':
        spike_matrix[:,:-D] = 1
        if 2 in current_spike_matrix:
            X=np.where(current_spike_matrix==2)[0]
            Y=np.where(current_spike_matrix==2)[1]
            for x,y in zip(X,Y):
                if y+D<current_spike_matrix.shape[0]:
                    if current_spike_matrix[x,y+D]==0:
                        spike_matrix[x,y]=2
    elif direction == 'L2R':
        # Tracer()()
        spike_matrix[:,D:] = 1
        if 2 in current_spike_matrix:
            X=np.where(current_spike_matrix==2)[0]
            Y=np.where(current_spike_matrix==2)[1]
            for x,y in zip(X,Y):
                if y-D>0:
                    if current_spike_matrix[x,y-D]==0:
                        spike_matrix[x,y]=2

    elif direction == 'U2D':
        spike_matrix[D:,:] = 1
        if 2 in current_spike_matrix:
            X=np.where(current_spike_matrix==2)[0]
            Y=np.where(current_spike_matrix==2)[1]
            for x,y in zip(X,Y):
                if x-D>0:
                    if current_spike_matrix[x-D,y]==0:
                        spike_matrix[x,y]=2

    return spike_matrix


class SerialListener():
    def __init__(self, baudrate=256000, NROWS=5, NCOLS=5, DEBUG=False, TIMESTAMPED=True):
        self.DEBUG = DEBUG
        self.TIMESTAMPED = TIMESTAMPED #for printing timestamps
        DUE_baudrate = 128000

        self.interFlagFrames = 0;

        # connect arduino
        arduino_MEGA_id = '95536333830351611222'  # '601A' # '95536333830351611222'
        arduino_DUE_id = '855313033313517101B0' # DUE doesnt like 256000 baud rate, 128000 is fine
        #dwenguino_id = '593335333538150F0A11'
        dwenguino_id = 'D3E0:601A'
        arduino_UNO_id = '2341:0043'
        #arduino_UNO_id = '7533030393435120D1F1'

        ids = [arduino_MEGA_id, dwenguino_id, arduino_DUE_id,arduino_UNO_id]

        #scan if any id present
        """
        uC_ports = [
            p[0] for p in serial.tools.list_ports.comports() if any(id in p[2] for id in ids)
        ]
        """
        uC_ports = []
        uC_ids = []
        for p in serial.tools.list_ports.comports():
            for id in ids:
                if id in p[2]:
                    uC_ports.append(p[0])
                    uC_ids.append(id)

        if not uC_ports:
            raise IOError("No uC found")

        self.ser = {'Ser':[],'sensorIDs':[],'interFlagFrames':[],'sensorTypes':[], 'sensorShapes':[], 'sensorNtaxels':[],
                    'resolution':[]}

        bytesize = 8
        parity = 'N'
        stopbits = 1
        timeout = 30

        serial_connections=[]
        for port,id in zip(uC_ports,uC_ids):
            if id == arduino_DUE_id:
                self.ser['Ser'].append(serial.Serial(port, baudrate=DUE_baudrate, bytesize=bytesize, parity=parity,
                                         stopbits=stopbits, write_timeout=3, timeout=timeout))
            else:
                serial_connections.append(serial.Serial(port, baudrate=baudrate, bytesize=bytesize, parity=parity,
                                         stopbits=stopbits, write_timeout=3, timeout=timeout))
            print('detected uC at ' + port)

        self.serial_instructions = {'get_period':5,'start':6,'stop':7,'demand_ID':8}
        self.identify_ser(serial_connections)
	

        # if len(uC_ports) > 1:
        #     print 'Multiple uCs found ('+str(ports)+')'




        # Counts=10
        # while Counts >0:
        #     try:
        #         self.ser = serial.Serial(uC_ports[0], baudrate=baudrate, write_timeout=3)
        #     except serial.serialutil.SerialException as e:
        #         if 'Device or resource busy' in e.args[1] :
        #             Counts-=1
        #             print 'uC found busy, will try '+str(Counts)+' more times'
        #             time.sleep(5)
        #             continue
        #         else:
        #             raise


        # self.AMOUNT_ROWS_TO_READ = NROWS
        # self.AMOUNT_COLS_TO_READ = NCOLS
        # self.AMOUNT_TAXELS = NROWS*NCOLS
        self.COUNT = 0
        self.N_MutantMatrices = 0

        return

    def read_line_serial(self):
        """

        :return: a list of ints for a single serial line
        """

        if self.TIMESTAMPED:
            t0 = time.time()
        data_str = self.ser.readline()
        if self.TIMESTAMPED:
            t1 = time.time()
        if self.DEBUG:
            print('raw data_str:')
            print(data_str)

        data_list = data_str.rstrip().split(',')
        if self.TIMESTAMPED:
            t2= time.time()
        if data_list == ['']:

            if self.COUNT != self.AMOUNT_ROWS_TO_READ:
                self.N_MutantMatrices += 1
            if self.DEBUG:
                print('COUNT: ',self.COUNT)
                print('N_MutantMatrices: ', self.N_MutantMatrices)
            self.COUNT = 0
            data = np.NaN
        else:
            data = data_list[:-1]  # [int(x) for x in data_str[:-1]] #seems to be a '' at end of every row
            self.COUNT += 1
        if self.TIMESTAMPED:
            t3 = time.time()
            print('read_line_serial: ' + str(t1 - t0) + '+' + str(t2 - t1) + '+' + str(t3 - t2) + ' = ' + str(t3-t0))
        return data

    def read_serial(self):
        """

        :return: a list of ints for a single serial line
        """

        if self.TIMESTAMPED:
            t0 = time.time()
        print('start serial reading')


        # data = []
        # for i in range(self.AMOUNT_TAXELS):
        #     data.append(self.ser.read(1))

        while True:
            b = self.ser.read(1)
            print(ord(b),type(b))
            if b == " ":
                print('check')

        #data_str = self.ser.readline()

        if self.TIMESTAMPED:
            t1 = time.time()
            print('read_serial: ' + str(t1 - t0))
        data=data.reshape((self.AMOUNT_ROWS_TO_READ,self.AMOUNT_COLS_TO_READ))
        return data

    def kickstart_frame_reading(self):
        """
        Skip few frames
        Seems like the first read frames can be irregular...
        :return:
        """
        line = self.read_line_serial()
        while line == line:  # skip till nan
            line = self.read_line_serial()
        line = self.read_line_serial()
        while line == line:  # skip till nan
            line = self.read_line_serial()
        line = self.read_line_serial()
        while line == line:  # skip till nan
            line = self.read_line_serial()
        return

    def identify_ser(self,serial_connections):
        """
        identify if a touch sensor is attached by looking for flag signal, otherwise assume distance/pressure sensor
        :return:
        """

        for Ser in serial_connections:
            print('identifying '+str(Ser))
            time.sleep(3)

            #Tracer()()
            if Ser.inWaiting() > 0: #touch sensor
                count = 0
                max_reads = 64*150
                while count < max_reads:
                    try:# sometimes an empty string appears, which ord does not like
                        b = ord(Ser.read(1))
                    except:
                        print('second try ...')
                        b = ord(Ser.read(1))

                    print(count, b),
                    if b == 99:
                        flagTail = [ord(Ser.read(1)),ord(Ser.read(1))]
                        if flagTail == [33,66]:  # [99,33,66] is used as 'flag' sequence to synchronize and followed by info
                            interFlagFrames = ord(Ser.read(1))
                            sensorID = ord(Ser.read(1))
                            sensorShape = tuple([ord(Ser.read(1)),ord(Ser.read(1))])
                            resolution = ord(Ser.read(1))
                            self.ser['Ser'].append(Ser)
                            self.ser['interFlagFrames'].append(interFlagFrames)
                            self.ser['sensorIDs'].append(sensorID)
                            self.ser['sensorShapes'].append(sensorShape)
                            self.ser['sensorNtaxels'].append(sensorShape[0]*sensorShape[1])
                            self.ser['sensorTypes'].append('touch')
                            self.ser['resolution'].append(resolution)
                            break
                    count+=1
            if Ser.inWaiting() == 0: #demand ID to see if other sensor type
                print(' wil demand ID')
                Ser.write(str(chr(self.serial_instructions['demand_ID'])))
                print('demanded ID')
                for i in range(10):
                    print('no reply, try again')
                    time.sleep(1)
                    Ser.write(str(chr(self.serial_instructions['demand_ID'])))
                    try:
                        b = ord(Ser.read(1))
                        break
                    except:
                        continue
                if b==49:
                    self.ser['Ser'].append(Ser)
                    self.ser['sensorTypes'].append('distance')
                    self.ser['sensorIDs'].append(b)
                    self.ser['interFlagFrames'].append(np.NaN)
                    self.ser['sensorShapes'].append(np.NaN)
                    self.ser['sensorNtaxels'].append(np.NaN)
                    self.ser['resolution'].append(np.NaN)
                elif b==16:
                    self.ser['Ser'].append(Ser)
                    self.ser['sensorTypes'].append('pressure')
                    self.ser['sensorIDs'].append(b)
                    self.ser['interFlagFrames'].append(np.NaN)
                    self.ser['sensorShapes'].append(np.NaN)
                    self.ser['sensorNtaxels'].append(np.NaN)
                    self.ser['resolution'].append(np.NaN)
                elif b == 19:
                    self.ser['Ser'].append(Ser)
                    self.ser['sensorTypes'].append('servo')
                    self.ser['sensorIDs'].append(b)
                    self.ser['interFlagFrames'].append(np.NaN)
                    self.ser['sensorShapes'].append(np.NaN)
                    self.ser['sensorNtaxels'].append(np.NaN)
                    self.ser['resolution'].append(np.NaN)
                elif b == 68:
                    self.ser['Ser'].append(Ser)
                    self.ser['sensorTypes'].append('distanceServo')
                    self.ser['sensorIDs'].append(68)
                    self.ser['interFlagFrames'].append(np.NaN)
                    self.ser['sensorShapes'].append(np.NaN)
                    self.ser['sensorNtaxels'].append(np.NaN)
                    self.ser['resolution'].append(np.NaN)
                    '''
                    self.ser['Ser'].append(Ser)
                    self.ser['sensorTypes'].append('distance')
                    self.ser['sensorIDs'].append(49)
                    self.ser['interFlagFrames'].append(np.NaN)
                    self.ser['sensorShapes'].append(np.NaN)
                    self.ser['sensorNtaxels'].append(np.NaN)
                    self.ser['resolution'].append(np.NaN)
                    self.ser['Ser'].append(Ser)
                    self.ser['sensorTypes'].append('servo')
                    self.ser['sensorIDs'].append(19)
                    self.ser['interFlagFrames'].append(np.NaN)
                    self.ser['sensorShapes'].append(np.NaN)
                    self.ser['sensorNtaxels'].append(np.NaN)
                    self.ser['resolution'].append(np.NaN)
                    '''
                else:
                    raise Exception('Sensor not identified')
        print(' identified: ',self.ser['sensorTypes'],'with IDs: ',self.ser['sensorIDs'])
        Ser.reset_input_buffer()
        return

    def read_flag(self, i,verbose=False):
        """

        :param i: Index of Serial connection
        :return:
        """
        while True:
            b = ord(self.ser['Ser'][i].read(1))
            if verbose:
                print('waiting for flag')
            if b == 99:
                flagTail = [ord(self.ser['Ser'][i].read(1)),ord(self.ser['Ser'][i].read(1))]
                if flagTail == [33,66]:  # [99,33,66] is used as 'flag' sequence to synchronize and followed by info
                    interFlagFrames = ord(self.ser['Ser'][i].read(1))
                    sensorID = ord(self.ser['Ser'][i].read(1))
                    sensorShape = tuple([ord(self.ser['Ser'][i].read(1)), ord(self.ser['Ser'][i].read(1))])
                    resolution = ord(self.ser['Ser'][i].read(1))
                    if verbose:
                        print('Flag check')
                        print('interFlagFrames = ' + str(interFlagFrames))
                        print('sensorID = ' + str(sensorID))
                    break
                else:
                    continue
                    # print flagTail
                    # print i
                    # raise Exception('Flag not recognized/ out of sync')
        return interFlagFrames,sensorID

    def read_loop(self,i):
        while True:
            if self.ser['Ser'][i].inWaiting() > 31:
                print('in waiting: ')
                print(self.ser['Ser'][i].inWaiting())
                dd = self.ser['Ser'][i].readline()#(self.ser['sensorNtaxels'][i])
                print('line length: ')
                print(len(dd))
                data = np.array([ord(d) for d in dd[:-1]])
                print('data: ')
                print(data)
        return

    def read_frame2(self, i):
        """

        :param i: Index of Serial connection
        :return:
        """

        Ntaxels = self.ser['sensorNtaxels'][i]
        if self.ser['resolution'][i]==10:
            N_expected = Ntaxels*2
        else:
            N_expected = Ntaxels


        data = []
        for j in range(N_expected):
            data.append(ord(self.ser['Ser'][i].read(1)))

        data=np.array(data)
        # if 10 bit
        if self.ser['resolution'][i] == 10:
            LB = data[:Ntaxels]
            #print 'LB: '
            #print LB
            HB = data[Ntaxels:] * 2 ** 8
            #print 'HB :'
            #print HB
            data = LB + HB
        #print 'data: '
        #print data
        return data.reshape(self.ser['sensorShapes'][i])

    """
    def read_frame(self):

        if self.TIMESTAMPED:
            t0 =time.time()

        data_matrix = np.zeros((self.AMOUNT_ROWS_TO_READ, self.AMOUNT_COLS_TO_READ))

        if self.TIMESTAMPED:
            t1 =time.time()

        line = self.read_serial()
        while line == line:  # skip till nan
            line = self.read_line_serial()
        print 'found nan'
        if self.TIMESTAMPED:
            t2 = time.time()

        SensorID = 66#self.read_line_serial()[0] #first line after nan = sensorID


        if self.DEBUG:
            print 'SensorID = '+str(SensorID)

        if self.TIMESTAMPED:
            t3 = time.time()

        amount_reads = 0
        while amount_reads < self.AMOUNT_ROWS_TO_READ:
            line = self.read_line_serial()
            if self.DEBUG:
                print 'line: ', line
            if line == line:  # if not nan
                if len(line) == self.AMOUNT_COLS_TO_READ:
                    data_matrix[amount_reads] = np.array(line).astype(int)

                amount_reads += 1
        if self.DEBUG:
            print 'data matrix: '
            print data_matrix

        if self.TIMESTAMPED:
            t4 = time.time()
            print ' whole read frame took: '+str(t4-t0)
            print ' which should equal: '+str(t1-t0)+'+'+str(t2-t1)+'+'+str(t3-t2)+'+'+str(t4-t3)

        return data_matrix
    """

    def save_data(self, exp_name, rec_dur=10):
        """
        rec_dur = desired duration of recording (seconds)

        """

        # for j in range(100):
        #     for i in range(len(self.ser)):
        #         data_matrix = self.read_frame2(i)
        #         print data_matrix



        exp_timestamp = GU.get_timestamp()
        data_files = []
        data_arrays = []
        timestamp_arrays = []
        #interFlagFrames = []

        #identify sensors
        #self.identify_ser()#in init

        print(self.ser)

        #create a savefile for each identified sensor
        for idx, Ser in enumerate(self.ser['Ser']):
            Ser.reset_input_buffer()

            #iFF, SensorID = self.read_flag(i, verbose=True)
            #interFlagFrames.append(iFF)

            name = exp_timestamp +'_'+ exp_name+'_ID'+str(self.ser['sensorIDs'][idx])
            exp_dir = os.environ['HOME'] +'/Documents/ExperimentData/FlexSensor'
            if not os.path.exists(exp_dir):
                os.mkdir(exp_dir)

            print('will save in '+exp_dir)

            filename=exp_dir + '/' + name


            if self.ser['sensorTypes'][idx] == 'touch':
                f = tables.open_file(filename, mode='w')
                NUM_COLUMNS = self.ser['sensorNtaxels'][idx]
                data_arrays.append(f.create_earray(f.root, 'data', tables.UInt16Atom(), (0, NUM_COLUMNS)))
                f.create_array(f.root, 'sensorshape', self.ser['sensorShapes'][idx])
            elif self.ser['sensorTypes'][idx] == 'distance':
                f = tables.open_file(filename, mode='w')
                data_arrays.append(f.create_earray(f.root, 'data', tables.Float32Atom(), (0, 1)))
                Ser.write(str(chr(self.serial_instructions['get_period'])))  # start signal
                P_distance = 0.001*ord(self.ser['Ser'][idx].read(1))# time (s) between data transmissions
                data_buffer = []
            elif self.ser['sensorTypes'][idx] == 'pressure':
                f = tables.open_file(filename, mode='w')
                data_arrays.append(f.create_earray(f.root, 'data', tables.Float32Atom(), (0, 1)))
                Ser.write(str(chr(self.serial_instructions['start'])))	#start signal
                print('sent start signal')
            elif self.ser['sensorTypes'][idx] == 'servo':
                f = tables.open_file(filename, mode='w')
                data_arrays.append(f.create_earray(f.root, 'data', tables.UInt8Atom(), (0, 1)))
            elif self.ser['sensorTypes'][idx] == 'distanceServo':
                # IR_dist_and_servo_UNO_v2a.ino
                # one uC will send data from both IR distance sensor and the desired Servo position
                # here  data will first be stored in an array and later be disentangled and put into an array
                # a dummy entry has to be made into data_arrays to preserve the indexing
                data_buffer = []
                timestamp_buffer = []
                data_arrays.append([])
                timestamp_arrays.append([])


            if not self.ser['sensorTypes'][idx] == 'distanceServo':
                timestamp_arrays.append(f.create_earray(f.root, 'timestamp_s', tables.Float32Atom(), (0, 1)))
                f.create_array(f.root, 'sensorID', self.ser['sensorIDs'][idx])
                f.create_array(f.root, 'sensorType', self.ser['sensorTypes'][idx])
                data_files.append(f)



        prev_timestamp = 0
        timestamp = 0
        lock_time = 0
        t0_distance = 0
        timestamp_servo = 0
        count=[]

        for Ser in self.ser['Ser']:
            count.append(0)
        print('starting recording:')

        if 'servo' in self.ser['sensorTypes']:
            servo_idx = self.ser['sensorTypes'].index('servo')
            self.ser['Ser'][servo_idx].write(str(chr(self.serial_instructions['start'])))
        elif 'distanceServo' in self.ser['sensorTypes']:
            distanceServo_idx = self.ser['sensorTypes'].index('distanceServo')
            self.ser['Ser'][distanceServo_idx].write(str(chr(self.serial_instructions['start'])))

        time0 = time.time()
        while timestamp < rec_dur:



            for idx in range(len(self.ser['Ser'])):
                t0 = time.time()
                if self.ser['sensorTypes'][idx] == 'touch':
                    if count[idx]==self.ser['interFlagFrames'][idx]:
                        a,b=self.read_flag(idx)
                        count[idx] = 0
                    else:
                        if self.ser['Ser'][idx].inWaiting()>=self.ser['sensorNtaxels'][idx]:

                            data_matrix = self.read_frame2(idx)
                            timestamp = time.time() - time0
                            dt = timestamp - prev_timestamp
                            prev_timestamp = timestamp
                            print('dt: ')
                            print(dt)
                            data_arrays[idx].append(data_matrix.reshape(1, -1))
                            timestamp_arrays[idx].append(np.array([[timestamp]]))
                            count[idx]+=1
                            framerate = data_arrays[idx].shape[0] / timestamp
                            sys.stdout.write('\r' + str(int(timestamp)) + 's/' + str(rec_dur) + 's, saving framerate = ' + str(
                                framerate) + ' Hz')
                            sys.stdout.flush()
                        #print 'frame '+str(count)
                elif self.ser['sensorTypes'][idx] == 'distance':
                    '''
                    print 'time : '+str(time.time()-lock_time)
                    print ' in waiting: ' +str(self.ser['Ser'][idx].in_waiting)
                    if self.ser['Ser'][idx].in_waiting>2:
                        self.ser['Ser'][idx].reset_input_buffer()
                        timestamp_distance=1
                    '''
                    if t0_distance == 0:
                        self.ser['Ser'][idx].write(str(chr(self.serial_instructions['start'])))  # start signal
                        print('sent start signal')
                        t0_distance = time.time()
                    N_data = self.ser['Ser'][idx].in_waiting
                    if N_data>40:
                        for i in range(N_data):
                            data_buffer.append(ord(self.ser['Ser'][idx].read(1)))
                        print(' data_buffer Hz: '+str((len(data_buffer)/2)/(time.time()-time0)))
                        '''
                        if self.ser['Ser'][idx].in_waiting==2:

                            """
                            self.ser['Ser'][idx].reset_input_buffer()
                            value = ord(self.ser['Ser'][idx].read(1))
                            while value != 66: #skip to flag
                                value = ord(self.ser['Ser'][idx].read(1))
                            """
                            #write ID to sensor to demand data
                            #self.ser['Ser'][idx].write(str(chr(self.ser['sensorIDs'][idx])))
                            HB = ord(self.ser['Ser'][idx].read(1)) #High Byte
                            #print HB
                            LB = ord(self.ser['Ser'][idx].read(1)) #Low Byte
                            #print LB
                            voltage = (HB*2**8 + LB)/100.
                            print 'voltage :'+str(voltage)
                            data_arrays[idx].append(np.array([[voltage]]))
                            timestamp_distance = time.time() - time0
                            timestamp_arrays[idx].append(np.array([[timestamp_distance]]))
                            lock_time = time.time()
                        else:
                            print ' in waiting: ' +str(self.ser['Ser'][idx].in_waiting)
                        '''

                elif self.ser['sensorTypes'][idx] == 'pressure':
                    print(self.ser['sensorTypes'])
                    if self.ser['Ser'][idx].in_waiting>3:
                        while self.ser['Ser'][idx].in_waiting>5:#dont read old data
                            self.ser['Ser'][idx].read(1)
                        value = ord(self.ser['Ser'][idx].read(1))
                        while value != 66: #skip to flag
                            value = ord(self.ser['Ser'][idx].read(1))
                        HB = ord(self.ser['Ser'][idx].read(1)) #High Byte
                        LB = ord(self.ser['Ser'][idx].read(1)) #Low Byte
                        pressure = HB*2**8 + LB
                        data_arrays[idx].append(np.array([[pressure]]))
                        time1 = time.time()
                        timestamp_pressure = time1 - time0
                        timestamp_arrays[idx].append(np.array([[timestamp_pressure]]))

                elif self.ser['sensorTypes'][idx] == 'servo':
                    if time.time()-lock_time>0.001:
                        #print ' dt since lock: '+str(time.time()-lock_time)
                        if self.ser['Ser'][idx].in_waiting>0:
                            print(' dt interead: '+ str(time.time()-lock_time))
                            Byte = ord(self.ser['Ser'][idx].read(1))
                            data_arrays[idx].append(np.array([[Byte]]))
                            timestamp_servo = time.time()-time0
                            timestamp_arrays[idx].append(np.array([[timestamp_servo]]))
                            lock_time = time.time()

                elif self.ser['sensorTypes'][idx] == 'distanceServo':
                    if time.time()-lock_time>0.001:
                        #print ' dt since lock: '+str(time.time()-lock_time)
                        if self.ser['Ser'][idx].in_waiting>=4:
                            print(' dt interead: '+ str(time.time()-lock_time))
                            for i in range(4):
                                data_buffer.append(ord(self.ser['Ser'][idx].read(1)))
                            timestamp_distanceServo = time.time()-time0
                            timestamp_buffer.append(timestamp_distanceServo)
                            #data_arrays[idx].append(np.array([[Byte]]))
                            #timestamp_arrays[idx].append(np.array([[timestamp_distanceServo]]))
                            lock_time = time.time()


        for f in data_files:
            f.close()

        if 'servo' in self.ser['sensorTypes']:
            servo_idx = self.ser['sensorTypes'].index('servo')
            self.ser['Ser'][servo_idx].write(str(chr(self.serial_instructions['stop'])))

        elif 'distanceServo' in self.ser['sensorTypes']:
            ##send stop signal for servo
            distanceServo_idx = self.ser['sensorTypes'].index('distanceServo')
            self.ser['Ser'][distanceServo_idx].write(str(chr(self.serial_instructions['stop'])))

            ##create data files
            #distance sensor
            ID=49
            name = exp_timestamp +'_'+ exp_name+'_ID'+str(ID)
            filename=exp_dir + '/' + name
            f_distance = tables.open_file(filename, mode='w')
            distance_earray = f_distance.create_earray(f_distance.root, 'data', tables.Float32Atom(), (0, 1))
            distance_t_earray = f_distance.create_earray(f_distance.root, 'timestamp_s', tables.Float32Atom(), (0, 1))
            f_distance.create_array(f_distance.root, 'sensorID', ID)
            f_distance.create_array(f_distance.root, 'sensorType', 'distance')

            #servo postion
            ID=19
            name = exp_timestamp +'_'+ exp_name+'_ID'+str(ID)
            filename=exp_dir + '/' + name
            f_servo = tables.open_file(filename, mode='w')
            servo_earray = f_servo.create_earray(f_servo.root, 'data', tables.UInt8Atom(), (0, 1))
            servo_t_earray = f_servo.create_earray(f_servo.root, 'timestamp_s', tables.Float32Atom(), (0, 1))
            f_servo.create_array(f_servo.root, 'sensorID', ID)
            f_servo.create_array(f_servo.root, 'sensorType', 'servo')

            ##store data
            #find first flag
            for idx, itm in enumerate(data_buffer):
                if itm == 66:  # could be flag
                    if data_buffer[idx + 4] == 66 and data_buffer[idx + 4 * 10] == 66:  # confirm flag
                        startidx = idx
                        break

            data_buffer = data_buffer[startidx:]
            data_buffer.reverse() # reverse in order to use pop()
            timestamp_buffer = timestamp_buffer[startidx:]
            timestamp_buffer.reverse() # reverse in order to use pop()
            #Tracer()()
            while len(data_buffer)>=4:
                flag = data_buffer.pop()
                HB = data_buffer.pop()
                LB = data_buffer.pop()
                servo = data_buffer.pop()
                timestamp = timestamp_buffer.pop()

                voltage = (HB * 2 ** 8 + LB) / 100.
                distance_earray.append(np.array([[voltage]]))
                distance_t_earray.append(np.array([[timestamp]]))

                servo_earray.append(np.array([[servo]]))
                servo_t_earray.append(np.array([[timestamp]]))
            f_distance.close()
            f_servo.close()

        '''
        print 'len data_buffer :' + str(len(data_buffer))
        print ''
        for idx, itm in enumerate(data_buffer):
            if itm == 66: #could be flag
                if data_buffer[idx+4]==66 and data_buffer[idx+4*10]==66: #confirm flag
                    startidx = idx
                    break
        '''
        '''
        t_diff = t0_distance-time0
        distance_idx = self.ser['sensorTypes'].index('distance')
        self.ser['Ser'][idx].write(str(chr(self.serial_instructions['stop'])))  # stopsignal
        servo_idx = self.ser['sensorTypes'].index('servo')
        for idx, itm in enumerate(data_buffer[startidx:]):
            it = idx%4
            if it==0 :
                continue
            if it==1:
                HB=itm
            if it==2:
                LB=itm
                voltage = (HB * 2 ** 8 + LB) / 100.
                timestamp = (startidx+idx)/4 * 2*P_distance #ever P_distance seconds, alternatingly 1 or 3 bytes are sent
                timestamp+=t_diff
                data_arrays[distance_idx].append(np.array([[voltage]]))
                timestamp_arrays[distance_idx].append(np.array([[timestamp]]))
            if it ==3:
                data_arrays[servo_idx].append(np.array([[itm]]))
                timestamp = (startidx+idx)/2 * P_distance #ever P_distance seconds, alternatingly 1 or 3 bytes are sent
                timestamp+=t_diff
                timestamp_arrays[servo_idx].append(np.array([[timestamp]]))
        '''


        print('saved data in '+filename)
        return

    def print_to_terminal(self):
        count = 0
        time0= time.time()
        while True:
            data_matrix = self.read_frame()
            print(data_matrix)
            count+=1
            print('fr: '+str(count/(time.time()-time0)))
        return

class Visualiser():
    def __init__(self, dataFile=None):

        if dataFile:
            data_file_dir = dataFile
            data_file=tables.open_file(data_file_dir,mode='r',title='ReadData')
            print(data_file_dir)
            self.NROWS = data_file.root.sensorshape[0]
            self.NCOLS = data_file.root.sensorshape[1]
            self.data = np.array(data_file.root.data).reshape(-1, self.NROWS, self.NCOLS)
            self.timestamps = np.array(data_file.root.timestamp_s).reshape(-1,)

            print('data shape: '+ str(self.data.shape))

            self.N_frames = self.data.shape[0]
            self.recording_fr = self.N_frames/self.timestamps[-1]
        else:
            self.Ser = SerialListener()#(NROWS=self.NROWS,NCOLS=self.NCOLS)
            for idx, itm in enumerate(self.Ser.ser['sensorTypes']):
                if itm == 'touch':
                    self.sensorIdx = idx
            self.NROWS = self.Ser.ser['sensorShapes'][self.sensorIdx][0]
            self.NCOLS = self.Ser.ser['sensorShapes'][self.sensorIdx][1]
            self.base_frame = np.zeros((self.NROWS,self.NCOLS))
            N=10
            frames = np.zeros((N,self.NROWS,self.NCOLS))

            for i in range(N):
                a, b = self.Ser.read_flag(self.sensorIdx)
                frames[i] = self.Ser.read_frame2(self.sensorIdx)
            self.base_frame = np.average(frames,axis=0).astype(np.int64)
            print(frames)
            print(self.base_frame)
        self.prev_frame = np.zeros((self.NROWS,self.NCOLS))

        print(' init finit ')
        return

    def make_spike_matrix(self, curr_frame):

        diff = curr_frame.astype('int32') - self.prev_frame.astype('int32') #needs to be a signed and large enough type
        spike_matrix = np.zeros((self.NROWS,self.NCOLS))
        spike_matrix[diff>1] = 1
        self.prev_frame = curr_frame

        # if self.spike_mode=='thresholded':
        return spike_matrix

        # if self.spike_mode == 'motion':



    def update_plot_imshow(self,i):


        #self.ax.cla()  # clear the plot
        

        #nx, ny = self.NROWS, self.NCOLS
        if i<self.N_frames:
            data_matrix = self.data[i]
            self.ax.imshow(data_matrix,cmap=cm.jet,interpolation='none')

            spike_matrix = self.make_spike_matrix(data_matrix)

            self.ax_spike.imshow(spike_matrix,cmap='gray',interpolation='none')
            print(spike_matrix)

            self.ax_spike.set_xlabel('frame '+str(i)+'/'+str(self.N_frames))
        else:
            self.ax_spike.set_xlabel('frame ' + str(self.N_frames) + '/' + str(self.N_frames))


        # print "read data took" + str(time1-time0)
        # print "analog imshow took"+ str(time2-time1)
        # print "make spike matrix took"+ str(time3-time2)
        # print "spike inmshow took"+ str(time4-time3)
        # print "update_plot_imshow took" +str(time5-time0)
        return

    def animate_spiking(self):

        # matplotlib settings

        ##mpl.rcParams['legend.fontsize'] = 10

        print('init ani')

        # set up plot area
        fig = plt.figure()
        plt.suptitle('Taxel Grid',fontsize=18)
        ##fig.canvas.mpl_connect('close_event', self.close) #add close event
        self.ax = fig.add_subplot(211)#, projection='3d')
        self.ax.set_title('analog mode')
        #self.ax.hold(False) # discards the old graph
	

        self.ax_spike = fig.add_subplot(212)
        self.ax_spike.set_title('spiking mode')
        #self.ani = animation.FuncAnimation(fig, func=self.update_plot, interval=20)
        self.ani = animation.FuncAnimation(fig, func=self.update_plot_imshow, interval=10)
        #ani_spike = animation.FuncAnimation(fig_spike, func=self.update_plot_spike, interval=20)
        #plt.show(block=True)

        print('return ani')
        return #self.ani


    def update_plot_3D(self,i):

        i=i*30 #because blit doesnt want to work with surface plot

        self.ax.cla()  # clear the plot
        plt.suptitle('Recording frequency = ' + str(np.round(self.recording_fr,1))+ 'Hz')
        self.ax.set_zlim([-30, 1020])
        if i<self.N_frames:
            self.ax.set_title('frame '+str(i)+'/'+str(self.N_frames)+', '+str(self.timestamps[i])+'/'+str(self.timestamps[-1])+'s')

            while True:
                data_matrix = self.data[i]
                if 255 in data_matrix:
                    i+=1
                    data_matrix = self.data[i]
                else:
                    break

            surf = self.ax.plot_surface(self.X, self.Y, data_matrix.T, cmap=cm.jet, rstride=1, cstride=1, vmin=0, vmax=800, shade=False)
            surf.set_facecolor((0, 0, 0, 0))
            self.ax.set_zlim([-30, 1020])
            #surf._facecolors2d = surf._facecolors3d #for blit to work but not sufficient
            #surf._edgecolors2d = surf._edgecolors3d
            return surf,
        else:
            self.ax.set_title('finished')
        # print 'update plot 3D took '+str(time.time()-t0)+'s'
        # print 't1-t0 took'+str(t1-t0)
        # print 't2-t1 took' + str(t2-t1)
        # print 't3-t2 took' + str(t3-t2)
        # print 'inter function time: '+str(t0-self.prev_end_t)
        return

    def init_plot_3D(self):
        data_matrix = self.data[0]
        surf = self.ax.plot_surface(self.X, self.Y, data_matrix.T, cmap=cm.jet, rstride=1, cstride=1, vmin=0, vmax=40,
                                    shade=False)
        plt.suptitle('Recording frequency = '+str(self.recording_fr))
        return surf,

    def animate_3D(self,fps=20):

        fig = plt.figure()
        self.ax = fig.add_subplot(111, projection='3d')  # create an axis
        self.ax.hold(False)  # discards the old graph
        #self.circle = Circle((0,0), 1.0)
        #self.ax.add_artist(self.circle)
        #self.ax.set_xlim([0,10])
        #self.ax.set_ylim([-2,2])

        x = range(self.NROWS)
        y = range(self.NCOLS)

        self.X, self.Y = np.meshgrid(x, y)  # `plot_surface` expects `x` and `y` data to be 2D

        interval = 1000/fps
        self.anim = animation.FuncAnimation(fig,self.update_plot_3D,interval=interval)#interval in ms, blit doesnt work, needs init func and must return iterable artists
        return self.anim


    def update_plot_live(self,i):

        idx = self.sensorIdx
        # if self.framecount == self.Ser.ser['interFlagFrames'][idx]:
        #     a, b = self.Ser.read_flag(idx)
        #     self.framecount = 0
        # else:

        self.Ser.ser['Ser'][idx].reset_input_buffer()
        #time.sleep(0.004)
        a, b = self.Ser.read_flag(idx)
        data_matrix = self.Ser.read_frame2(idx)
        #data_matrix -= self.base_frame
        print(data_matrix)
        self.framecount += 1

        #self.Ser.ser.reset_input_buffer()
        #data_matrix = self.Ser.read_frame()

        self.ax.cla()  # clear the plot
        self.ax.set_title('Live Feed')
        if not self.AUTOSCALE:
            self.ax.set_zlim([-30, 1020])

        y = range(self.Ser.ser['sensorShapes'][idx][0])
        x = range(self.Ser.ser['sensorShapes'][idx][1])

        X, Y = np.meshgrid(x, y)  # `plot_surface` expects `x` and `y` data to be 2D

        Z=data_matrix

        surf = self.ax.plot_surface(X, Y, Z, cmap=cm.jet, rstride=1, cstride=1, vmin=0, vmax=800, shade=False)
        surf.set_facecolor((0, 0, 0, 0))
        
        return
    
    def animate_live(self, AUTOSCALE=False):

        self.AUTOSCALE = AUTOSCALE

        #self.Ser.kickstart_frame_reading()
        #self.N_MutantMatrices = 0

        # matplotlib settings
        #mpl.rcParams['legend.fontsize'] = 10

        self.framecount = 0
        # set up plot area
        fig = plt.figure()
        self.ax = fig.add_subplot(111, projection='3d')

        self.anim = animation.FuncAnimation(fig, func=self.update_plot_live, interval=20)
        
        return self.anim


    def close(self):
        print('flushing plt')
        plt.clf()
        plt.cla()
        plt.close()
        return

class Processor():
    def __init__(self, dataFile):

        self.data_file_dir = dataFile
        data_file=tables.open_file(self.data_file_dir,mode='r',title='ReadData')

        self.tag=self.data_file_dir[self.data_file_dir.find('2019'):]
        self.sensorID = data_file.root.sensorID.read()
        self.sensorshape = data_file.root.sensorshape.read()
        self.NROWS = self.sensorshape[0]
        self.NCOLS = self.sensorshape[1]
        self.Ntaxels = self.NROWS*self.NCOLS

        self.data = np.array(data_file.root.data).reshape(-1, self.NROWS, self.NCOLS)
        self.timestamps = np.array(data_file.root.timestamp_s).reshape(-1, )
        self.fs = self.timestamps.shape[0]/self.timestamps[-1]
        data_file.close()

        print('data shape: '+ str(self.data.shape))

        self.N_frames = self.data.shape[0]

        self.prev_frame = np.zeros((self.NROWS,self.NCOLS))

        ## check if ID49 present (distance sensor)
        self.tentative_file = dataFile[:dataFile.find('ID')]+'ID49'
        if os.path.isfile(self.tentative_file):
            print('found distance sensor datafile')
            data_file = tables.open_file(self.tentative_file, mode='r', title='ReadData')
            self.raw_distance_data=np.array(data_file.root.data).reshape(-1)
            self.distance_data = self.volt2m(self.clean_distance_data(self.raw_distance_data))
            self.distance_timestamps = np.array(data_file.root.timestamp_s).reshape(-1)
            self.distance_sensorID = data_file.root.sensorID.read() # should be 49
            data_file.close()
        ## check if ID19 present (servo sensor)
        self.tentative_file = dataFile[:dataFile.find('ID')] + 'ID19'
        if os.path.isfile(self.tentative_file):
            print('found servo datafile')
            data_file = tables.open_file(self.tentative_file, mode='r', title='ReadData')
            self.raw_servo_data = np.array(data_file.root.data).reshape(-1)
            self.servo_data = self.servo2m(self.raw_servo_data)
            self.servo_timestamps = np.array(data_file.root.timestamp_s).reshape(-1)
            self.servo_sensorID = data_file.root.sensorID.read()  # should be 19
            data_file.close()
        ## check if ID666 present (pneumatic pressure sensor)
        self.tentative_file = dataFile[:dataFile.find('ID')]+'ID666'
        if os.path.isfile(self.tentative_file):
            print('found pressure sensor datafile')
            data_file = tables.open_file(self.tentative_file, mode='r', title='ReadData')
            self.pressure_data=np.array(data_file.root.data).reshape(-1)
            self.pressure_timestamps = np.array(data_file.root.timestamp_s).reshape(-1)
            self.pressure_sensorID = data_file.root.sensorID.read() # should be 666
            data_file.close()

        print(' init finit ')
        return

    def get_mean_std(self):

        mean_series = []
        std_series = []
        #Tracer()()
        i=0
        while i<self.data.shape[0]:
            #print str(i)+'/'+str(self.data.shape[0])
            data_matrix = self.data[i]#.reshape((self.NROWS,self.NCOLS))

            ## this should be done before, independently
            #if 255 in data_matrix:
            #    i+=1
            #    continue

            mean = np.mean(data_matrix)
            std = np.std(data_matrix)

            mean_series.append(mean)
            std_series.append(std)
            i+=1

        return np.array(mean_series), np.array(std_series)

    def mean_row_column(self):
        row_series = []
        column_series = []
        times = []

        i=0
        while i<self.data.shape[0]:
            data_matrix = self.data[i].reshape((self.NROWS,self.NCOLS))

            #if 255 in data_matrix:
            #    i+=1
            #    continue

            #Tracer()()
            row = np.mean(data_matrix[:,2:],axis=1)
            column = np.mean(data_matrix,axis=0)

            row_series.append(row)
            column_series.append(column)
            times.append(self.timestamps[i])
            i+=1

        return np.array(row_series), np.array(column_series), np.array(times)

    def clean_distance_data(self,IR_sensor_output):
        x_clean=IR_sensor_output.copy()
        for idx,x in enumerate(IR_sensor_output):
            if x>5:
                x_clean[idx]=x_clean[idx-1]
        return x_clean

    def plot_calibration(self):

        start_t,start_idc = self.servo_start_timestamps(0.0005)
        colors = np.random.rand(len(start_t),3)
        fig = plt.figure()
        ax1=fig.add_subplot(211)
        ax1.plot(self.servo_timestamps,self.servo_data)
        for idx in range(len(start_t)-1):
            ax1.axvspan(start_t[idx], start_t[idx + 1], alpha=0.3, color=colors[idx])
        ax2=fig.add_subplot(212,sharex=ax1)
        ax2.plot(self.distance_timestamps,self.raw_distance_data)
        for idx in range(len(start_t) - 1):
            ax2.axvspan(start_t[idx], start_t[idx + 1], alpha=0.3, color=colors[idx])
        plt.show()
        return

    def volt2m(self, IR_sensor_output):

        ## calibration data measured with ruler
        #V_calib = np.array([4.61,4.58,4.51,4.36,4.21,4.02,3.91,3.79,3.65,3.53,3.41,3.32,3.19,3.05,2.94,2.88,2.77,2.74,2.69,2.63,2.52,2.42,2.42,2.37,2.32,2.2,2.17,2.15,2.15,2.09,2.06,1.97,1.94,1.88,1.82,0.45])
        #mm_calib = np.array([44,46,48,50,52,54,56,58,60,62,64,66,68,70,72,74,76,78,80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,220])

        ## calibration data based on servo position
        mm_calib = np.array([0.0, 1.0, 2.3, 3.5, 5.1, 6.9, 8.5, 10.1, 12.7,15.1,17.6,20.2,22.9,25.7,28.6,31.4,34.3,37.1,39.8,42.3,44.7,46.9])
        V_calib = np.array([1.20, 1.22, 1.24, 1.28, 1.30,1.33,1.35,1.39,1.41,1.45,1.49,1.55,1.59,1.66,1.72,1.78,1.86,1.94,2.00,2.08,2.15,2.19])
        #add edge cases for interpolation
        mm_calib = np.concatenate(([0.],mm_calib,[53.]))
        V_calib = np.concatenate(([-0.1],V_calib,[2.8]))

        f = sp.interpolate.interp1d(V_calib,mm_calib)
        mm_data = f(IR_sensor_output)
        return mm_data*0.001

    def servo2m(self,S):

        #S = servo value (in range [0,255])

        servo_val_min = 25.
        servo_val_max = 150.
        theta_min = 104. #256. #min corner (ccw)
        theta_max = 237. #123. #max corner (ccw)

        step_size = (theta_max-theta_min)/(servo_val_max-servo_val_min) #degree per servo step

        n_steps = servo_val_max-S #counterclockwise n steps
        theta = theta_min + n_steps*step_size

        #(x1,y1) is coordinate of first joint (servo arm) wrt servo
        #x1,x2 = self.servo2coord(S)
        r = 0.0285
        x1,y1= r*np.cos(theta*np.pi/180.),r*np.sin(theta*np.pi/180.)

        #b = sqrt(a**2-c**2)
        a = 0.074 #length connection piece
        d = 0.002 #distance on x axis between servo center and corner cb (the 90degree corner)
        c = abs(x1)+d
        b = np.sqrt(a**2-c**2)

        #(x2,y2) is coordinate of second joint (inside moving box) wrt servo
        x2,y2=-d,b+y1

        Min_distance = 0.04799 # 0.04799 for servo_val_min of 25
        distance=y2-Min_distance # relative to lowest postion

        return distance

    def clean_servo_data(self,servo_data):
        x_clean=servo_data.copy()
        diff=x_clean[:-1]-x_clean[1:]
        diff_idcs=np.where(diff>10)[0]
        for idx in diff_idcs:
            x_clean[idx+1]=x_clean[idx]
        plt.figure()
        plt.plot(x_clean)
        return x_clean

    def detect_slip(self, N_gauss=9, sigma_gaus = 2, peak_threshold=0.07):
        """

        :param N_gauss: width of the gaussian window , int
        :param sigma_gaus: sigma of the gaussian window , int
        :param peak_threshold: threshold of differential above which slip peak is detected, float
        :return:
        """
        # t = obj.distance_timestamps
        # dd = distance_data
        ## convolve with gaussian to smooth
        convoluted_dd = self.gaussian_convolution(self.distance_data, N=N_gauss, s=sigma_gaus)
        ## take differential to detect movement along y axis
        diff_dd = np.diff(convoluted_dd)
        ## peaks = local max > peak_threshold
        local_max = sps.argrelextrema(diff_dd, np.greater)
        mask = diff_dd[local_max] > peak_threshold
        peaks = local_max[0][mask]

        ## move left and right from peaks to get all slip timestamps
        slip_idcs = []
        peak_lower_threshold = 0.015
        for peak in peaks:
            slip_idcs_left = []
            diff_idx = peak
            diff = diff_dd[diff_idx]
            #move to left until bottom peak
            while diff > peak_lower_threshold:
                slip_idcs_left.append(diff_idx)
                diff_idx -= 1
                diff = diff_dd[diff_idx]
            slip_idcs_right = []
            diff_idx = peak+1
            diff = diff_dd[diff_idx]
            #move to right until bottom peak
            while diff > peak_lower_threshold:
                slip_idcs_right.append(diff_idx)
                diff_idx +=1
                diff = diff_dd[diff_idx]
            slip_idcs+=slip_idcs_left+slip_idcs_right

        #remove overlap
        slip_idcs = np.unique(slip_idcs)

        #binary slip signal
        y = np.zeros(self.distance_data.shape)
        y[slip_idcs] += 1

        slip_idcs_bounds = []
        prev = 0
        for idx, itm in enumerate(y):
            if abs(prev-itm)>0:
                slip_idcs_bounds.append(idx)
            prev = itm

        return slip_idcs, slip_idcs_bounds, y

    def analog2spiking(self, analog_data, spike_threshold):
        """

        :param analog_data: data consisting of frames, shape = (Nframes, Nrows, Ncols)
        :return:
        """
        ##initialize
        spiking_data = []
        prev_frame = np.zeros(analog_data[0].shape)
        ## iterate analog frames:
        for frame in analog_data:
            spike_matrix = analog2diff(frame, prev_frame, spike_threshold=spike_threshold)
            spiking_data.append(spike_matrix)
            prev_frame = frame
        return np.array(spiking_data)

    def gaussian_convolution(self,data,N,s):
        """

        :param N: N samples gaussian window
        :param s: sigma gaussian
        :return:
        """
        gaussian_window = sps.gaussian(N, s) / sum(sps.gaussian(N, s))
        convolved_data = np.convolve(data, gaussian_window,'same')
        return convolved_data

    def edit_recording(self,removeMax=False,cutBounds=[None,None],reference_file=None):
        """
        incomplete, does not edit ID666 nor transfers metadata
        cutBounds = [start,end] ; start and end time
        reference_file : directory to file containing reference recording, to be subtracted from recording
        """



        # filename = self.data_file_dir+'_edited'

        id_idx = self.data_file_dir.find('ID')
        filename = self.data_file_dir[:id_idx]+'edited_'+self.data_file_dir[id_idx:]

        if cutBounds[0]:
            lb_idx=np.argmin(abs(self.timestamps-cutBounds[0]))
        else:
            lb_idx = None
        if cutBounds[1]:
            ub_idx=np.argmin(abs(self.timestamps-cutBounds[1]))
        else:
            ub_idx =None

        # slice data
        data=self.data[lb_idx:ub_idx] #if None nothing is cut
        timestamps = self.timestamps[lb_idx:ub_idx] #if None nothing is cut

        print(data.shape)
        NUM_COLUMNS = data.shape[1]
        NUM_ROWS = data.shape[0]

        with tables.open_file(filename, mode='w') as f:
            atom = tables.Int16Atom() #UInt8Atom()  # UInt32Atom()#Float64Atom()
            array_c = f.create_earray(f.root, 'data', atom, (0, 1*self.NCOLS))
            array_stamp = f.create_earray(f.root, 'timestamp_s', tables.Float32Atom(), (0, 1))


            if reference_file:
                proc = Processor(reference_file)
                ref_frame = np.average(proc.data[200:201],axis=0).astype(np.uint64)

            print('start editing:')

            # iterate over sliced data, processing can happen here
            for frame, stamp in zip(data,timestamps):
                if removeMax:
                    if 255 in frame:
                        print('continue')
                        continue

                if reference_file:
                    frame-=ref_frame

                array_c.append(frame.reshape(1,-1))
                array_stamp.append(np.array([[stamp]]))

            ##copy metadata
            f.create_array(f.root, 'sensorshape', self.sensorshape)
            f.create_array(f.root, 'sensorID', self.sensorID)
            f.create_array(f.root, 'sensorType', 'touch')

            f.close()
            print('saved data in '+filename)

        ##same treatment to distance data (ID49) if exist
        if hasattr(self, 'distance_data'):

            filename = self.data_file_dir[:id_idx]+'edited_'+'ID49'

            if cutBounds[0]:
                lb_idx = np.argmin(abs(self.distance_timestamps - cutBounds[0]))
            else:
                lb_idx = None
            if cutBounds[1]:
                ub_idx = np.argmin(abs(self.distance_timestamps - cutBounds[1]))
            else:
                ub_idx = None

            # slice data
            data = self.raw_distance_data[lb_idx:ub_idx]
            timestamps = self.distance_timestamps[lb_idx:ub_idx]

            with tables.open_file(filename, mode='w') as f:

                arrays_c = f.create_earray(f.root, 'data', tables.Float32Atom(), (0, 1))
                array_stamp = f.create_earray(f.root, 'timestamp_s', tables.Float32Atom(), (0, 1))

                # iterate over sliced data, processing can happen here
                for distance, stamp in zip(data,timestamps):
                    arrays_c.append(np.array([[distance]]))
                    array_stamp.append(np.array([[stamp]]))

                # copy metadata
                f.create_array(f.root, 'sensorID', self.distance_sensorID)
                f.create_array(f.root, 'sensorType', 'distance')

                f.close()

            print('saved data in '+filename)


        ##same treatment to servo data (ID19) if exist
        if hasattr(self, 'servo_data'):

            filename = self.data_file_dir[:id_idx]+'edited_'+'ID19'

            if cutBounds[0]:
                lb_idx = np.argmin(abs(self.servo_timestamps - cutBounds[0]))
            else:
                lb_idx = None
            if cutBounds[1]:
                ub_idx = np.argmin(abs(self.servo_timestamps - cutBounds[1]))
            else:
                ub_idx = None

            # slice data
            data = self.raw_servo_data[lb_idx:ub_idx]
            timestamps = self.servo_timestamps[lb_idx:ub_idx]

            with tables.open_file(filename, mode='w') as f:

                arrays_c = f.create_earray(f.root, 'data', tables.UInt8Atom(), (0, 1))
                array_stamp = f.create_earray(f.root, 'timestamp_s', tables.Float32Atom(), (0, 1))

                # iterate over sliced data, processing can happen here
                for pos, stamp in zip(data,timestamps):
                    arrays_c.append(np.array([[pos]]))
                    array_stamp.append(np.array([[stamp]]))

                # copy metadata
                f.create_array(f.root, 'sensorID', self.servo_sensorID)
                f.create_array(f.root, 'sensorType', 'servo')

                f.close()

            print('saved data in '+filename)

        return

    def plot_all_traces(self,inverseRowCol=False):

        fig = plt.figure()
        colors = ['blue', 'orange', 'green', 'cyan', 'purple', 'black', 'yellow', 'red', 'grey']

        if inverseRowCol:
            data=np.transpose(self.data,axes=[0,2,1]).reshape(-1,self.Ntaxels)
            NplotCols = self.NROWS
            NplotRows = self.NCOLS
            figshape = NplotRows*100+11
            label='column '
        else:
            data=self.data.reshape(-1, self.Ntaxels)
            NplotCols=self.NCOLS
            NplotRows = self.NROWS
            figshape = NplotRows*100+11
            label='row '


        for idx, trace in enumerate(data.T):
            #Tracer()()
            cidx = idx / NplotCols
            if idx % NplotCols == 0:  # new row new subplot
                if cidx == 0:
                    ax = fig.add_subplot(figshape + cidx)
                else:
                    ax = fig.add_subplot(figshape + cidx, sharex=ax)
                ax.plot(self.timestamps, trace, color=colors[cidx], label=label + str(cidx))
                ax.legend()
                if type(self.data[0][0][0]) == np.uint16:
                    ax.set_ylabel('Sensor digital readout [0,1023]')
                    ax.set_ylim(0, 1023)
                if type(self.data[0][0][0]) == np.uint8:
                    ax.set_ylabel('Sensor digital readout [0,254]')
                    ax.set_ylim(0, 254)
            else:
                ax.plot(self.timestamps, trace, color=colors[cidx])

        ax.set_xlabel('time (s)')
        plt.suptitle('All traces, ' + self.tag)
        plt.show()

        return fig

    ######################################
    #FlexFinger experiments
    ######################################

    def pressure_start_stop_timestamps(self):
        count=0
        starts=[]
        stops=[]
        for x0, x1 in zip(self.pressure_data[:-1],self.pressure_data[1:]):
            if x0<15 and x1>40:
                t=self.pressure_timestamps[count]
                if len(starts) == 0:
                    starts.append(t)
                elif t>starts[-1]+10 :
                    starts.append(t)
                else:
                    print('discarded t='+str(t))
            count+=1
        start_idcs = []
        for start in starts:
            idx = np.argmin(abs(self.timestamps-start))
            start_idcs.append(idx)
        return starts, start_idcs

    def average_window(self,window_edges,VERBOSE_PLOT=False):
        # plt.figure()
        windows=[]
        window_dur=min([t1 - t0 for t1, t0 in zip(window_edges[1:], window_edges[:-1])])
        xnew=np.arange(0.0001,window_dur-0.005,0.003)
        interp_windows=np.empty((len(window_edges)-1,len(xnew),self.NROWS,self.NCOLS))
        count=0
        for t0,t1 in zip(window_edges[:-1],window_edges[1:]):
            idx0=np.argmin(abs(self.timestamps-t0))
            idx1=np.argmin(abs(self.timestamps-t1))
            window=self.data[idx0:idx1]
            windows.append(window)
            t=self.timestamps[idx0:idx1]-self.timestamps[idx0]
            for row in range(self.NROWS):
                for col in range(self.NCOLS):
                    y=window[:,row,col]
                    f = interp1d(t, y)
                    interp_window = f(xnew)
                    interp_windows[count,:,row,col]=interp_window
            #plt.plot(t,y)
            count+=1

        avg_window = np.average(interp_windows,axis=0)

        if VERBOSE_PLOT:
            fig=plt.figure()
            j=0
            for row in range(self.NROWS):
                for col in range(self.NCOLS):
                    j+=1
                    if j==1:
                        ax=fig.add_subplot(6,4,j)
                    else:
                        ax=fig.add_subplot(6,4,j,sharex=fig.get_axes()[0],sharey=fig.get_axes()[0])
                    for i in range(interp_windows.shape[0]):
                        ax.plot(xnew, interp_windows[i, :, row, col],color='grey')
                    ax.plot(xnew,avg_window[:,row,col],color='black')
                    if row==self.NROWS-1:
                        ax.set_xlabel('time (s)')
                    else:
                        plt.setp(ax.get_xticklabels(),visible=False)
                    if col==0:
                        ax.set_ylabel('readout')
                    else:
                        plt.setp(ax.get_yticklabels(),visible=False)
            plt.suptitle(self.tag+' (black=avg)')
            plt.show()
        return windows, interp_windows, avg_window, xnew

    def plot_labels(self,Fig=None,labels=None,fileName=None):
        """

        :param Fig: figure
        :param labels: list of tuples [(start_time,end_time),...]
        :param fileName: dir to .npy file
        :return:
        """
        #TODO : automate label finding

        if fileName:
            # dd=np.load(os.environ['HOME']+'/Dropbox/UGent/ExperimentData/FlexSensor/EdgeDetection_250619/VID_20190625_095752349_labels.npy')
            labels=np.load(fileName)
        elif labels:
            pass
        else:
            raise Exception('You have to give me something :D')

        if Fig:
            for ax in Fig.get_axes():
                # min,max = ax.get_ylim()[0],ax.get_ylim()[1]
                for x in labels:
                    # ax.plot([x[0], x[0], x[1], x[1]], [min, max, max, min], color='red', linestyle='--', linewidth=1)
                    ax.axvspan(x[0], x[1], alpha=0.3, color='red')
        else:
            plt.figure()
            for x in labels:
                # plt.plot([x[0], x[0], x[1], x[1]], [0, 1500, 1500, 0], color='red', linestyle='--', linewidth=1)
                plt.axvspan(x[0], x[1], alpha=0.3, color='red')
        plt.show()


        """
        dd = np.load(os.environ[
                          'HOME'] + '/Dropbox/UGent/ExperimentData/FlexSensor/EdgeDetection_250619/VID_20190625_110109181_2019-6-25-11-1-11_ref_air_labels.npy')

        if Fig:
            for ax in Fig.get_axes():
                for x in dd:
                    ax.plot([x[0], x[0], x[1], x[1]], [0, 1500, 1500, 0], color='black', linestyle='--', linewidth=1)
                    ax.axvspan(x[0], x[1], alpha=0.3, color='grey')

        else:
                for x in dd:
                    plt.plot([x[0], x[0], x[1], x[1]], [0, 1500, 1500, 0], color='black', linestyle='--', linewidth=1)
                    plt.axvspan(x[0], x[1], alpha=0.3, color='grey')
        """
        return


    def plot_pressure_starts(self):
        starts, start_idcs = self.pressure_start_stop_timestamps()
        fig = self.plot_all_traces()
        plt.vlines(starts,0,3000)
        plt.show()
        return

    def subtract_reference(self,PLOT=False):
        #ref = np.load(os.environ['HOME']+'/Dropbox/UGent/ExperimentData/FlexSensor/EdgeDetection_250619/avg_ref_air.npy')
        ref = np.load(os.environ['HOME'] + '/Dropbox/UGent/ExperimentData/FlexSensor/EdgeDetection_250619/avg_ref_towel.npy')
        starts,start_idcs = self.pressure_start_stop_timestamps()
        data_subtr = self.data.copy().astype(np.float64)
        refLen = ref.shape[0]
        #Tracer()()

        ## first make an interpolation function for the stored reference
        maxLen=np.max(np.diff(starts,1))
        for row in range(self.NROWS):
            for col in range(self.NCOLS):
                y = ref[:, row, col]
                t = np.linspace(0,maxLen, len(y))  # max possible t range
                f = interp1d(t, y)
                ##now, iterate over intervals and subtract interpolated ref
                for idx0, idx1 in zip(start_idcs[:-1],start_idcs[1:]):
                    xnew = self.timestamps[idx0:idx1] - self.timestamps[idx0]
                    interp_ref = f(xnew)
                    data_subtr[idx0:idx1,row,col]=data_subtr[idx0:idx1,row,col]-interp_ref

        if PLOT:
            fig = plt.figure()
            colors = ['blue', 'orange', 'green', 'cyan', 'purple', 'black', 'yellow', 'red', 'grey']
            for idx, trace in enumerate(data_subtr.reshape(-1, self.Ntaxels).T):
                cidx = idx / self.NCOLS
                if idx % self.NCOLS ==0: #new row new subplot
                    if cidx==0:
                        ax = fig.add_subplot(611 + cidx)
                    else:
                        ax = fig.add_subplot(611+cidx,sharex=ax)
                    ax.plot(self.timestamps, trace, color=colors[cidx], label='row ' + str(cidx))
                    ax.legend()
                    ax.set_ylabel('Readout')
                else:
                    ax.plot(self.timestamps, trace, color=colors[cidx])
                ax.set_ylim(ymin=-600,ymax=600)#-600,600 -150,100
                self.plot_labels()
            ax.set_xlabel('time (s)')

            plt.suptitle('Reference subtracted, ' + self.tag)
            plt.show()
        return data_subtr

    def plot_pressure_data(self,input_fig=None):
        if input_fig:
            fig = input_fig
            ax = fig.add_subplot(111, sharex=fig.get_axes()[0], frameon=False)
            ax.yaxis.tick_right()
            ax.yaxis.set_label_position("right")
            ax.plot(self.pressure_timestamps,self.pressure_data)
            #ax.tick_params(axis='y', colors='red')
            #ax.yaxis.label.set_color('red')
        else:
            fig=plt.figure()
            plt.plot(self.pressure_timestamps,self.pressure_data,'*')

        plt.xlabel('time (s)')
        plt.ylabel('pressure ')
        plt.show()
        return

    def plot_attractor(self):
        starts, start_idcs = self.pressure_start_stop_timestamps()
        dd = np.load(os.environ[
                         'HOME'] + '/Dropbox/UGent/ExperimentData/FlexSensor/EdgeDetection_250619/VID_20190625_095752349_labels.npy')

        subtr_data = self.subtract_reference()

        fig=plt.figure()
        for idx0, idx1 in zip(start_idcs[:-1], start_idcs[1:])[0:1]:
            plt.plot(subtr_data[idx0:idx1,4,1],subtr_data[idx0:idx1,5,1],color='black')

        for label in dd:
            # fig = plt.figure()
            idx0=np.argmin(abs(self.timestamps-label[0]))
            idx1=np.argmin(abs(self.timestamps-label[1]))
            plt.plot(subtr_data[idx0:idx1,4,1],subtr_data[idx0:idx1,5,1],color='red')
        plt.ylabel('row 5')
        plt.xlabel('row 4')
        plt.show()
        return

    ######################################
    #SensorSetup experiments
    ######################################

    def servo_start_timestamps_threshold(self,step_threshold):
        """

        :param step_threshold: in mm
        :return: indices indicating servo step
        """
        diff=self.servo_data[:-1]-self.servo_data[1:]
        step_idcs=list(np.where(abs(diff)>step_threshold)[0])
        step_times=self.servo_timestamps[step_idcs]
        #starts = [step_idcs[0]]
        #ends = []
        #for x0,x1 in zip(step_idcs[:-1],step_idcs[1:]): #remove adjacent detections
            # Tracer()()
            # if x1-x0>1:
                # ends.append(x0)
                # starts.append(x1)
        #ends.append(x1)
        return step_times, step_idcs

    def servo_start_timestamps_neighbours(self,distance=50):
        """

        :param distance: distance (in samples) to check for flatness
        :return:
        """
        start_stop_idc = []
        start_stop_times = []
        MOVING = False
        for idx, x in enumerate(self.servo_data[:-distance]):

            if self.checkleft(idx,distance) and not self.checkright(idx,1):
                start_idx = idx
                start_time = self.servo_timestamps[idx]
                MOVING = True
            if MOVING: #look for end of flatness
                if not self.checkleft(idx,1) and self.checkright(idx,distance):
                    stop_idx = idx
                    stop_time = self.servo_timestamps[idx]
                    start_stop_idc.append((start_idx,stop_idx))
                    start_stop_times.append((start_time,stop_time))
            if idx == self.servo_data.shape[0]-distance-1: #last sample
                if MOVING:
                    stop_idx = idx+distance
                    stop_time = self.servo_timestamps[idx+distance]
                    start_stop_idc.append((start_idx, stop_idx))
                    start_stop_times.append((start_time, stop_time))


        return start_stop_idc,start_stop_times



    def checkright(self,idx,N):
        X = self.servo_data[idx]
        if X==self.servo_data[idx+N]:
            if X==self.servo_data[idx+(N/2)]:
                return True
        else:
            return False

    def checkleft(self,idx,N):
        X = self.servo_data[idx]
        if X == self.servo_data[idx-N]:
            if X == self.servo_data[idx-(N/2)]:
                return True
        else:
            return False

    def plot_mean_with_histogram(self,spike_thresholds=[1],spike_frames_to_skip=0,histogram=False):
        """
        creates a figure with subplots, upper plot is the overall mean activity, other
        subplots for spiking versions according to thresholds in spike_thresholds
        :param spike_thresholds: list of spike thresholds used in conversion from analog2spiking
        :param histogram: set to True if histogram of spiking activity desired
        :return:
        """
        N_plots = 1+len(spike_thresholds)
        subplotbase = N_plots*100+10

        fig = plt.figure()
        self.plot_mean_distance(fig,subplotbase+1)
        for idx, spike_threshold in enumerate(spike_thresholds):
            self.plot_spike_histogram(spike_threshold,spike_frames_to_skip,fig,subplotbase+idx+2,histogram=histogram)
        #Tracer()()

        #make all x axes shared
        axes = fig.get_axes()
        for ax in axes[2:]:
            ax.get_shared_x_axes().join(ax, axes[0])
        if hasattr(self,'slip_start_idcs'):
            #plot slip detections on all subplots
            for ax in axes[2:]:
                ax.vlines(self.distance_timestamps[self.slip_start_idcs], -2, 20, color='red', linestyle='--')
        #remove xlabels
        for ax in axes[:-1]:
            ax.set_xlabel('')

        #reset xlims so that all subplots are plotted with same xlims from the start
        xlims = axes[0].get_xlim()
        axes[0].set_xlim(xlims[0],xlims[1])


        plt.show()
        return fig

    def plot_axes(self,plots=['mean_activity','spike_histogram']):

        fig = plt.figure()
        self.plot_mean_distance(fig,211)
        self.plot_spike_histogram(1,fig,212)
        plt.show()

    def plot_spike_histogram(self,spike_threshold=1,frames_to_skip=0,input_fig=None,subplotN=None,histogram=False):


        spiking_data = self.create_spiking_data(spike_threshold=spike_threshold,frames_to_skip=frames_to_skip)
        self._plot_spike_histogram(spiking_data,spike_threshold=spike_threshold,frames_to_skip=frames_to_skip,input_fig=input_fig,subplotN=subplotN,histogram=histogram)


    def _plot_spike_histogram(self,spiking_data,spike_threshold=None,frames_to_skip=0,input_fig=None,subplotN=None,histogram=False):


        all_spike_timestamps = []
        if not input_fig:
            fig0 = plt.figure()
            ax1 = fig0.add_subplot(111)
        else:
            ax1 = input_fig.add_subplot(subplotN,axisbg='grey')
        for ID,st in enumerate(spiking_data.T): #st=spiketrain
            pos_spikes=np.where(st==2)[0]
            neg_spikes=np.where(st==0)[0]
            y = np.ones((pos_spikes.shape)) * ID
            # Tracer()()
            x_pos = self.timestamps[pos_spikes]#*(frames_to_skip+1)]
            ax1.plot(x_pos, y, '|', color='white', mew=2)
            y = np.ones((neg_spikes.shape)) * ID
            x_neg = self.timestamps[neg_spikes]#*(frames_to_skip+1)]
            ax1.plot(x_neg, y, '|', color='black', mew=2)
            for x in x_pos:
                all_spike_timestamps.append(x)
            for x in x_neg:
                all_spike_timestamps.append(x)


        ax1.set_xlim(self.timestamps[0], self.timestamps[-1])
        # ax1.set_ylim(-0.5, input_size - 0.5)
        ax1.set_ylabel('Neuron ID')
        if spike_threshold:
            ax1.set_title('spike threshold: '+str(spike_threshold)+', skipped frames: '+str(frames_to_skip))
        ax1.set_xlabel('time (s)')

        if histogram:
            if not input_fig:
                ax2 = fig0.add_subplot(111, sharex=ax1, frameon=False)
            else:
                ax2 = input_fig.add_subplot(subplotN, sharex=ax1, frameon=False)
            bin_width = 0.3 #in s
            n_bins = int(max(all_spike_timestamps)/0.3)
            ax2.hist(all_spike_timestamps,bins=n_bins,color='black')
            ax2.yaxis.tick_right()
            ax2.yaxis.set_label_position("right")

        #ax1.text(self.timestamps[-1]/2,-2,'spike threshold: '+str(spike_threshold))
        if not input_fig:
            plt.show()
        return

    def plot_mean_distance(self,input_fig=None,subplotN=None):
        mean_series, std_series = self.get_mean_std()
        times =self.timestamps

        """
        plt.figure()
        plt.plot(times,mean_series,label='mean matrix activity')
        plt.plot(self.distance_timestamps,self.distance_data,label='distance')
        plt.xlabel('time (s)')
        plt.ylabel('Sensor digital readout [0,254]')
        plt.legend(loc=0)
        plt.show()
        """

        # create the general figure

        # and the first axes using subplot populated with data
        if not input_fig:
            fig1 = plt.figure()
            ax1 = fig1.add_subplot(111)
        else:
            ax1 = input_fig.add_subplot(subplotN)

        ax1.plot(times,mean_series,label='mean matrix activity',color='black')
        ax1.set_ylabel('Sensor mean readout [0,254]')
        ax1.set_xlabel('time (s)')


        # now, the second axes that shares the x-axis with the ax1
        if not input_fig:
            ax2 = fig1.add_subplot(111, sharex=ax1, frameon=False)
        else:
            ax2 = input_fig.add_subplot(subplotN, sharex=ax1, frameon=False)
        if hasattr(self,'servo_data'):
            ax2.plot(self.servo_timestamps, self.servo_data*1000, label='servo position', color='red')
            ax2.set_ylabel('servo position (mm)')
        elif hasattr(self,'distance_data'):
            ax2.plot(self.distance_timestamps,self.distance_data,label='distance sensor',color='red')
            ax2.set_ylabel('Distance from sensor (m)')
            if np.max(self.distance_data)>120:
                ax2.set_ylim(40,120)
            else:
                ax2.set_ylim(40)
            ax2.set_ylim(ax2.get_ylim()[::-1]) #invert y axis
            smooth_distance = self.gaussian_convolution(self.distance_data,N=9,s=2)
            ax2.plot(self.distance_timestamps,smooth_distance,color='purple')
            dt=0.1 # in s
            dy=0.3 # slip = dy that happens within dt
            dt_refr=0.5 #refractory period after slip detection in which no new slips can be detected
            fs_distance = self.distance_timestamps.shape[0]/self.distance_timestamps[-1]
            dt_N=int(dt*fs_distance)
            refr_N = int(dt_refr*fs_distance)
            end_touch_sensor=100 #in mm, point where we know object is past touch sensor

            slip_start_idcs = [0]
            idx=0
            for y0,y1 in zip(smooth_distance[:-dt_N],smooth_distance[dt_N:]):
                if y0 < end_touch_sensor:
                    if abs(y0-y1)>dy:
                        prev_idx = slip_start_idcs[-1]
                        if idx-prev_idx> refr_N: #past refractory period:
                            slip_start_idcs.append(idx)
                idx+=1

            slip_stop_idcs = [0]
            idx = 0
            smooth_distance_ud=np.flipud(smooth_distance.reshape(-1, 1))
            for y0, y1 in zip(smooth_distance_ud[:-dt_N], smooth_distance_ud[dt_N:]):
                if y0 < end_touch_sensor:
                    if abs(y0 - y1) > dy:
                        prev_idx = slip_stop_idcs[-1]
                        if idx - prev_idx > refr_N:  # past refractory period:
                            slip_stop_idcs.append(idx)
                idx += 1


            #ax2.scatter(self.distance_timestamps[slip_start_idcs],self.distance_data[slip_start_idcs],color='blue')
            ax2.vlines(self.distance_timestamps[slip_start_idcs], 0, 200, color='red',linestyle='--')
            ax2.vlines(self.distance_timestamps[slip_stop_idcs], 0, 200, color='green',linestyle='--')
            self.slip_start_idcs = slip_start_idcs


        ax2.yaxis.tick_right()
        ax2.yaxis.set_label_position("right")
        ax2.tick_params(axis='y', colors='red')
        ax2.yaxis.label.set_color('red')





        #peaks = sps.find_peaks_cwt(-mean_series, np.arange(30, 100))
        #ax1.scatter(times[peaks], mean_series[peaks], color='gray')
        #ax1.vlines(times[peaks], 0, 200, color='grey')
        #plt.title('gray lines indicate peak detection on mean V signal')


        ## differential of distance data
        #DS = 10 #downsample factor
        #dist_diff = np.diff(self.distance_data[::DS])
        #t_diff = self.distance_timestamps[::DS][:-1]
        #peaks = sps.find_peaks_cwt(dist_diff, np.arange(4, 30))
        #ax1.scatter(t_diff[peaks], dist_diff[peaks], color='red')
        #ax1.vlines(t_diff[peaks], 0, 200, color='red')


        if not input_fig:
            plt.show()
        return

    def plot_meanColumn_distance(self):
        mean_row_series, mean_column_series, times = self.mean_row_column()

        print(mean_column_series.shape)
        """
        plt.figure()
        plt.plot(times,mean_series,label='mean matrix activity')
        plt.plot(self.distance_timestamps,self.distance_data,label='distance')
        plt.xlabel('time (s)')
        plt.ylabel('Sensor digital readout [0,254]')
        plt.legend(loc=0)
        plt.show()
        """

        ################################################################
        # create the general figure

        fig1 = plt.figure()

        # first acis
        ax1 = fig1.add_subplot(111)
        for idx,column in enumerate(mean_column_series.T):
            ax1.plot(times,column,label='column '+str(idx))
        ax1.set_ylabel('Sensor digital readout [0,254]')
        ax1.set_xlabel('time (s)')

        plt.legend(loc=0)

        #ax1.spines['right'].set_color('green')
        #ax1.tick_params(axis='y', colors='blue')
        #ax1.spines['left'].set_color('blue')
        #ax1.yaxis.label.set_color('blue')

        # now, the second axes that shares the x-axis with the ax1
        ax2 = fig1.add_subplot(111, sharex=ax1, frameon=False)
        if hasattr(self,'servo_data'):
            ax2.plot(self.servo_timestamps, self.servo_data*1000, label='servo position', color='red')
            ax2.set_ylabel('servo position (mm)')
        elif hasattr(self,'distance_data'):
            ax2.plot(self.distance_timestamps,self.distance_data,label='distance sensor',color='red')
            ax2.set_ylabel('Distance from sensor (mm)')
            if np.max(self.distance_data)>120:
                ax2.set_ylim(40,120)
            else:
                ax2.set_ylim(40)
            ax2.set_ylim(ax2.get_ylim()[::-1]) #invert y axis

        ax2.yaxis.tick_right()
        ax2.yaxis.set_label_position("right")
        # ax2.spines['bottom'].set_color('green')
        # ax2.spines['top'].set_color('green')
        ax2.tick_params(axis='y', colors='red')
        ax2.yaxis.label.set_color('red')

        plt.title('mean column activity')
        plt.show()


        ################################################################

        diff = np.diff(mean_column_series, axis=0)
        threshold = 3
        above = diff > threshold
        crossing_idcs = []
        crossing_idcs.append(np.where(above)[0][0])
        for x, y in zip(np.where(above)[0][:-1], np.where(above)[0][1:]):
            if y - x > 1:
                crossing_idcs.append(y)

        fig2 = plt.figure()

        #first axis
        ax1 = fig2.add_subplot(111)
        for idx,column in enumerate(diff.T):
            ax1.plot(times[:-1],column,label='column '+str(idx))
        ax1.hlines(threshold,times[0],times[-1],color='black',linestyles='--',linewidth=2)
        ax1.scatter(times[crossing_idcs], np.ones(len(crossing_idcs))*threshold, label='threshold', color='gray')
        ax1.vlines(times[crossing_idcs], 0, 1.5*np.max(mean_column_series), color='grey')
        plt.title('threshold='+str(threshold)+' , grey lines indicate threshold passing')

        ax1.set_ylabel('differential avg column readout')
        ax1.set_xlabel('time (s)')


        #ax1.spines['right'].set_color('green')
        #ax1.tick_params(axis='y', colors='blue')
        #ax1.spines['left'].set_color('blue')
        #ax1.yaxis.label.set_color('blue')

        # now, the second axes that shares the x-axis with the ax1
        ax2 = fig2.add_subplot(111, sharex=ax1, frameon=False)
        ax2.plot(self.distance_timestamps,self.distance_data,label='distance sensor',color='red')
        ax2.set_ylabel('Distance from sensor (mm)')
        ax2.yaxis.tick_right()
        ax2.yaxis.set_label_position("right")
        if np.max(self.distance_data)>120:
            ax2.set_ylim(40,120)
        else:
            ax2.set_ylim(40)
        ax2.set_ylim(ax2.get_ylim()[::-1]) #invert y axis


        # ax2.spines['bottom'].set_color('green')
        # ax2.spines['top'].set_color('green')
        ax2.tick_params(axis='y', colors='red')
        ax2.yaxis.label.set_color('red')

        plt.legend(loc=0)
        plt.show()

        ################################################################
        fig3 = plt.figure()

        # first acis
        ax1 = fig3.add_subplot(111)
        for idx,column in enumerate(mean_column_series.T):
            ax1.plot(times,column,label='column '+str(idx))
        ax1.set_ylabel('Sensor digital readout [0,254]')
        ax1.set_xlabel('time (s)')


        for column in mean_column_series.T:
            peaks = sps.find_peaks_cwt(-column, np.arange(60, 100))
            ax1.scatter(times[peaks], column[peaks], color='gray')
            ax1.vlines(times[peaks], 0, 300, color='grey')
        plt.title('gray lines indicate peak detection on a column signal')


        #ax1.spines['right'].set_color('green')
        #ax1.tick_params(axis='y', colors='blue')
        #ax1.spines['left'].set_color('blue')
        #ax1.yaxis.label.set_color('blue')

        # now, the second axes that shares the x-axis with the ax1
        ax2 = fig3.add_subplot(111, sharex=ax1, frameon=False)
        ax2.plot(self.distance_timestamps,self.distance_data,label='distance sensor',color='red')
        ax2.set_ylabel('Distance from sensor (mm)')
        ax2.yaxis.tick_right()
        ax2.yaxis.set_label_position("right")
        if np.max(self.distance_data)>120:
            ax2.set_ylim(40,120)
        else:
            ax2.set_ylim(40)
        ax2.set_ylim(ax2.get_ylim()[::-1]) #invert y axis

        # ax2.spines['bottom'].set_color('green')
        # ax2.spines['top'].set_color('green')
        ax2.tick_params(axis='y', colors='red')
        ax2.yaxis.label.set_color('red')

        plt.legend(loc=0)
        plt.show()

    def plot_servo_data(self):
        fig = plt.figure()
        plt.plot(self.servo_timestamps,self.servo_data*1000)
        plt.ylabel('mm (from lowest position)')
        plt.xlabel('time (s)')
        plt.title('servo data')
        plt.show()
        return fig

    def create_spiking_data(self,spike_threshold,frames_to_skip=0):
        ##initialize
        spiking_data = []
        prev_frame = self.data[0]
        shape = prev_frame.T.reshape(-1).shape
        slice=frames_to_skip+1
        ## iterate analog frames:
        for frame in self.data[::slice]:
            spike_matrix = analog2diff(frame, prev_frame, spike_threshold=spike_threshold)
            spiking_data.append(spike_matrix.T.reshape(-1))
            prev_frame = frame
            for i in range(frames_to_skip):
                if len(spiking_data)<len(self.data):
                    spiking_data.append(np.ones(shape))
        return np.array(spiking_data)

    def plot_distance_data(self):
        fig=plt.figure()
        plt.plot(self.servo_timestamps,self.servo_data)
        plt.plot(self.distance_timestamps,self.distance_data)
        plt.xlabel('time (s)')
        plt.ylabel('distance (m)')
        plt.show()
        return



class Predictor():
    def __init__(self, dataFile):

        self.proc = Processor(dataFile)



        self.x_analog = self.proc.data
        self.timestamps = self.proc.timestamps

        self.target = self.get_targets()
        """
        ## for fake data: get targets from datafile
        data_file = tables.open_file(dataFile, mode='r', title='ReadData')
        self.target = data_file.root.moving.read()
        data_file.close()
        """

        # plt.figure()
        # plt.plot(t_distance,smooth_dd-40)
        # plt.plot(t_distance[:-1],diff_dd)
        # plt.scatter(t_distance[slip_idcs_bounds],diff_dd[slip_idcs_bounds])
        # #plt.vlines(t_distance[slip_idcs_bounds],0,100)
        # plt.show()

        # plt.plot(t_distance,self.proc.distance_data)
        #plt.plot(t_distance[slip_idcs],diff_dd[slip_idcs],color='red')
        # plt.plot(t_distance, y)
        # plt.vlines(t_distance[slip_idcs_bounds],0,100)
        # for t0,t1 in zip(t_distance[slip_idcs_bounds][::2] , t_distance[slip_idcs_bounds][1::2]):
        #     plt.axvspan(t0, t1, color='y', alpha=0.5, lw=0)
        # plt.show()

        return

    def LogReg(self,spike_thresholds=[10],spike_frames_to_skip=0,randomSampling=True,equalSize=False,negSpikes=True,test_size=0.3,analog_get_2_frames=True,plotResult=True):

        x_analog=self.x_analog.copy()
        timestamps=self.timestamps.copy()
        target=self.target.copy()

        if spike_frames_to_skip!=0:
            slice=spike_frames_to_skip+1
            #? change to x_analog[::slice] ?
            x_analog = self.proc.data[::slice]
            timestamps = self.proc.timestamps[::slice]
            target = self.get_targets()[::slice]

        if analog_get_2_frames:
            x1=x_analog[:-1]
            x2=x_analog[1:]
            x_analog=np.concatenate((x1,x2),axis=2)
            target=target[:-1]

        if equalSize and not randomSampling:
            raise Exception('if equalSize desired then use randomSampling ')

        if equalSize:
            pos = np.where(target==1)[0]
            neg = np.where(target==0)[0]
            neg = np.random.choice(neg,pos.shape)
            target = np.concatenate((target[pos],target[neg]))
            x_analog = np.concatenate((x_analog[pos], x_analog[neg]))
            timestamps = np.concatenate((timestamps[pos], timestamps[neg]))

        if randomSampling:
            x_train, x_test, y_train, y_test = train_test_split(x_analog.reshape(x_analog.shape[0],-1), target,
                                                                test_size=test_size, random_state=0)
        else:
            N_train = x_analog.shape[0]*(1-test_size)
            x_analog=x_analog.reshape(-1, self.proc.Ntaxels)

            x_train = x_analog[:N_train]
            x_test = x_analog[N_train:]
            y_train = target[:N_train]
            y_test = target[N_train:]

        #x_train, x_test, y_train, y_test = train_test_split(self.x_analog.reshape(-1, self.proc.Ntaxels), self.target,
        #                                                    test_size=0.3, random_state=0)



        logreg = LogisticRegression()
        logreg.fit(x_train, y_train)
        predictions = logreg.predict(x_test)
        score = logreg.score(x_test, y_test)
        cm = metrics.confusion_matrix(y_test, predictions)
        print('ANALOG DATA')
        print('accuracy: ')
        print(score)
        print('confusion matrix:')
        print('predicted: 0   1')
        print('class 0',cm[0])
        print('class 1',cm[1])

        for threshold in spike_thresholds:

            x_spiking = self.proc.create_spiking_data(spike_threshold=threshold,frames_to_skip=spike_frames_to_skip)


            if not negSpikes:
                x_spiking[x_spiking==0]=2 #set neg spikes to pos
                x_spiking[x_spiking==1]=0 #set absent of spikes to 0

            if equalSize:
                x_spiking=np.concatenate((x_spiking[pos],x_spiking[neg]))

            if randomSampling:
                x_train, x_test, y_train, y_test, t_train, t_test = train_test_split(x_spiking.reshape(-1, self.proc.Ntaxels), target, timestamps,test_size=test_size, random_state=0)
            else:
                N_train = x_spiking.shape[0] * (1 - test_size)
                x_spiking=x_spiking.reshape(-1, self.proc.Ntaxels)

                x_train = x_spiking[:N_train]
                x_test = x_spiking[N_train:]
                y_train = target[:N_train]
                y_test = target[N_train:]
                t_train = timestamps[:N_train]
                t_test = timestamps[N_train:]

            #x_train, x_test, y_train, y_test = train_test_split(x_spiking.reshape(-1,self.proc.Ntaxels), self.target, test_size=0.3, random_state=0)
            logreg = LogisticRegression()
            logreg.fit(x_train, y_train)
            predictions = logreg.predict(x_test)
            score = logreg.score(x_test, y_test)
            # Tracer()()
            cm = metrics.confusion_matrix(y_test, predictions)
            print('SPIKING DATA, spike threshold: '+str(threshold))
            print('accuracy: ')
            print(score)
            print('confusion matrix: ')
            print('predicted: 0   1')
            print('class 0',cm[0])
            print('class 1',cm[1])


            if plotResult:
                self.rasterPlot(x_test,y_test,predictions,t_test,spike_thresholds=[threshold],frames_to_skip=spike_frames_to_skip,title='spike threshold: '+str(threshold))
        return

    def get_targets_slipdur(self,slipdur=0.083): #0.083s, blueTube based on video

        '''
        slip_idcs, slip_idcs_bounds, slip_bin = proc.detect_slip()
        target = []
        for t in t_x_analog:
            idx = np.argmin(abs(t_distance - t))
            target.append(slip_bin[idx])
        target = np.array(target)
        '''



        servo_start_times, servo_start_idcs = self.proc.servo_start_timestamps_threshold(step_threshold=0.0005)

        blueTube_slipDur = slipdur

        targets = []
        for t in self.proc.timestamps:
            diff = servo_start_times-t
            if any((diff<0) & (diff>-blueTube_slipDur)):
                targets.append(1)
            else:
                targets.append(0)

        # N_samples = self.proc.servo_timestamps.shape[0]
        # rec_dur = self.proc.servo_timestamps[-1]-self.proc.servo_timestamps[0]
        # servo_fs = N_samples/rec_dur
        # N_slipDur_samples = round(blueTube_slipDur*servo_fs)
        #
        # targets = np.zeros(N_samples)
        # for idx in servo_start_idcs:
        #     targets[idx:idx+N_slipDur_samples]=1

        return np.array(targets)

    def get_targets(self):

        targets = []

        #create labels, if signal doesnt change for 200ms (50 samples) servo is not moving
        for t in self.proc.timestamps:
            idx=np.argmin(abs(self.proc.servo_timestamps-t))
            if idx<=50:
                if self.checkright(idx,50):
                    targets.append(0)
                else:
                    targets.append(1)

            elif idx>=len(self.proc.servo_data)-50:
                if self.checkleft(idx,50):
                    targets.append(0)
                else:
                    targets.append(1)

            else:
                if self.checkright(idx,50) or self.checkleft(idx,50):
                    targets.append(0)
                else:
                    targets.append(1)

        return np.array(targets)

    def checkright(self,idx,N):
        X = self.proc.servo_data[idx]
        if X==self.proc.servo_data[idx+N]:
            if X==self.proc.servo_data[idx+(N/2)]:
                return True
        else:
            return False

    def checkleft(self,idx,N):
        X = self.proc.servo_data[idx]
        if X == self.proc.servo_data[idx-N]:
            if X == self.proc.servo_data[idx-(N/2)]:
                return True
        else:
            return False

    def plot_targets(self,spike_thresholds):

        fig=self.proc.plot_mean_with_histogram(spike_thresholds=spike_thresholds)
        for ax in fig.get_axes():
            max_y=ax.get_ylim()[1]*0.8
            ax.plot(self.proc.timestamps,self.target*max_y)
        return

    def rasterPlot(self,x_test,y_test,predictions,t_test,spike_thresholds,frames_to_skip,title=''):

        Tracer()()

        fig = self.proc.plot_mean_with_histogram(spike_thresholds=spike_thresholds,spike_frames_to_skip=frames_to_skip)
        ax=fig.get_axes()[-1]
        #plot labels
        max_y = ax.get_ylim()[1] * 0.8
        ax.plot(self.timestamps, self.target * max_y)
        #plot test results
        for idx,t in enumerate(t_test):
            if y_test[idx]==predictions[idx]:
                color='green'
            else:
                color='red'
            ax.axvspan(t-0.00025,t+0.00025,color=color,alpha=0.5,linewidth=1)

        # fig = plt.figure()
        #
        # ax1 = fig.add_subplot(111)
        # for ID,st in enumerate(x_test): #st=spiketrain
        #     pos_spikes=np.where(st==2)[0]
        #     # Tracer()()
        #     #neg_spikes=np.where(st==0)[0]
        #     #y = np.ones((pos_spikes.shape)) * ID
        #     #x_pos = self.timestamps[pos_spikes]
        #     x_pos = np.ones(len(pos_spikes))*ID
        #     ax1.plot(x_pos,pos_spikes, '|', color='blue', mew=2)
        #     #y = np.ones((neg_spikes.shape)) * ID
        #     #x_neg = self.timestamps[neg_spikes]
        #     #ax1.plot(x_neg, y, '|', color='orange', mew=2)
        #     #for x in x_pos:
        #     #    all_spike_timestamps.append(x)
        #     #for x in x_neg:
        #     #    all_spike_timestamps.append(x)
        # ax1.plot(y_test*x_test.shape[1]*0.8,color='black')
        # ax1.plot(predictions*x_test.shape[1]*0.8,color='red')
        # ax1.set_xlabel('N')
        # plt.title(title)
        # plt.show()
        return

if __name__ == "__main__":
    print("start")
    # start an animation that calls the update function

    ## ani = animation.FuncAnimation(fig, func=update, interval=10)
    # plt.show()
    #dataFile = '/home/alexander/Documents/ExperimentData/	FlexSensor/2019-1-25-17-42-39_test_flexSensor_dataSave'
    #Vis = Visualiser(dataFile)
    ##ani = Vis.animate_3D()
    #ani = Vis.animate_spiking()
    #plt.show()


