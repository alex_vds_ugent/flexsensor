//implementing uart slave
// for instructions see doc/Flexsensor_instructionsv2.pdf
// tested on ubuntu_usbASP_ArduinoIDE1.8.5
// tested on windows_arduinoIDE
#include <Wire.h>

/*  Change the address of the sensor here:
 *  Two groups of eight addresses (0000 XXX and 1111 XXX) are reserved for other purposes in the I2C protocol.
 *  Not allowed: {<0 ,[0, 7], [120, 127], >127}
 *  see https://www.nxp.com/docs/en/user-guide/UM10204.pdf, p17 for more information.
*/
const int I2C_ADDR = 11;
const int COLUMN_PINS[] =  {1, 0, 2, 3, 4};
const int ROW_PINS[] = {9, 10, 11, 13, 14};//, 15, 0, 1, 2, 3};

const int LEN_COLUMNS  = sizeof(COLUMN_PINS) / sizeof(int);
const int LEN_ROWS = sizeof(ROW_PINS) / sizeof(int);

int SensorRowToQuery = 0;
volatile int SensorValues[LEN_ROWS][LEN_COLUMNS];

char Sent2Master = 1;

void setup() {

  // init i2c
  Wire.begin(I2C_ADDR);                // join i2c bus with address
  Wire.onRequest(i2c_onRequest_callback);
  Wire.onReceive(i2c_onReceive_callback); 

  // init variabelen matrixen
  for (int i = 0; i < LEN_COLUMNS; i++) {
    for (int j = 0; j < LEN_ROWS; j++) {
      SensorValues[j][i] = 1;
    }
  }
}

void loop() {
  if (Sent2Master == 1){ // if data has been read by master over I2C
    getData();  
  }
}


void i2c_onReceive_callback(int n) {
  SensorRowToQuery = Wire.read(); // receive byte as a character
}

void i2c_onRequest_callback() {

  if (SensorRowToQuery==0){
    for (int j = 0; j < 5; j++) { //iter rows
      for (int k = 0; k < LEN_COLUMNS; k++) { //iter columns
        byte b = SensorValues[j][k] / 4;
        Wire.write(b);
      }
    }
  }
  if (SensorRowToQuery==1) {
    for (int j = 3; j < 5; j++) { //iter rows
      for (int k = 0; k < LEN_COLUMNS; k++) { //iter columns
        byte b = SensorValues[j][k] / 4;
        Wire.write(b);
      }
    }
   Sent2Master = 1; 
  }
}

void getData() {
  for (int i = 0; i < LEN_ROWS; i++) {
    
    //all rows high impedance
    for (int k = 0; k < LEN_ROWS; k++) {
      pinMode(ROW_PINS[k], INPUT);   // set digital pin as input
    }

    // turn on selected row
    pinMode(ROW_PINS[i], OUTPUT);   // set digital pin as output
    digitalWrite(ROW_PINS[i], HIGH);

    // read out analog inputs
    for (int j = 0; j < LEN_COLUMNS; j++) { 
      SensorValues[i][j] = analogRead(COLUMN_PINS[j]);
    }
    Sent2Master = 0; 
  }
}


